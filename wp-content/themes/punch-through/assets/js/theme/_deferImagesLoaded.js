/**
 *  Initialize image defer
 */
const deferImagesLoaded = (function() {

  const pub = {}; // public facing functions

  pub.init = function() {
    this._deferImageLoad();
    this._deferBackgroundImageLoad();
  };

  /**
   * Look for elements with a src data attribute and defer the images from loading until other resources are complete
   */
  pub._deferImageLoad = function() {
    const imgDefer = document.querySelectorAll('[data-src]');
    for (let i = 0; i < imgDefer.length; i++) {
      if (imgDefer[i].getAttribute('data-src')) {
        imgDefer[i].setAttribute('src', imgDefer[i].getAttribute('data-src'));
        imagesLoaded(imgDefer[i], {background: true}, function(instance, image) {
          this.elements[0].classList.add('image--loaded');
        });
      }
    }
  };

  /**
   * Look for elements with a bkg data attribute and defer the images from loading until other resources are complete
   */
  pub._deferBackgroundImageLoad = function() {
    const imgDefer = document.querySelectorAll('[data-bkg]');
    for (let i = 0; i < imgDefer.length; i++) {
      if (imgDefer[i].getAttribute('data-bkg')) {
        imgDefer[i].style.backgroundImage = "url('" + imgDefer[i].getAttribute('data-bkg') + "')";
        imagesLoaded(imgDefer[i], {background: true}, function(instance, image) {
          if (this.elements[0].classList.contains('header__bg-image') || this.elements[0].classList.contains('hero__background')) {
            this.elements[0].classList.add('background--transparent');
          }
          this.elements[0].classList.add('background--loaded');
        });
      }
    }
  };

  return pub;

}());
