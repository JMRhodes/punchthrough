<?php
/**
 * ACF Module: Hero
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline   = ACF::getField('headline', $data);
$content    = ACF::getField('content', $data);
$button     = ACF::getField('button', $data);
$button_alt = ACF::getField('button_alt', $data);
?>

<div id="<?php echo $row_id; ?>" class="module hero">
    <div class="module__background" <?php echo Util::getInlineStyles($data, 'featured--xl', true); ?>></div>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="hero__heading">
                    <h1 class="hero__title hdg hdg--1 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h1>
                </div>
                <div class="hero__body hdg--5 hdg--normal entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>

                <div class="btn__group">
                    <?php
                    echo Util::getButtonHTML($button);

                    echo Util::getButtonHTML($button_alt, ['class' => 'btn btn--secondary']);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

