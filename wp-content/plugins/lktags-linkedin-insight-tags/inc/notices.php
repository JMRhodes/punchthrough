<?php

// free only notices
// pro notification
function lktags_notice_subscribe()
{
    if ( !PAnD::is_admin_notice_active( 'lktags-subcribe-120' ) ) {
        return;
    }
    $purchase_url = "options-general.php?page=lktags-pricing";
    $getpro = sprintf( wp_kses( __( 'Get 10&#37; OFF on Pinterest Conversion Tag PRO if you subscribe here:', 'lktags' ), array(
        'a' => array(
        'href' => array(),
    ),
    ) ), esc_url( $purchase_url ) );
    ?>
        <div data-dismissible="lktags-subcribe-120" class="notice lktags-notice notice-success is-dismissible">
            <p class="lktags-p"><?php 
    echo  $getpro ;
    ?></p>
            <form action="https://Pagup.us14.list-manage.com/subscribe/post?u=a706b8e968389b05725c65849&amp;id=59ebd61587" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate lktags-form-notice" target="_blank" novalidate>

                <input type="email" value="" name="EMAIL" class="lktags-field-notice" placeholder="<?php 
    echo  __( 'Email address', 'lktags' ) ;
    ?>" required>

                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a706b8e968389b05725c65849_59ebd61587" tabindex="-1" value=""></div>
                <div class="clear lktags-clear-notice"><input type="submit" value="<?php 
    echo  __( 'Subscribe', 'lktags' ) ;
    ?>" name="subscribe" id="mc-embedded-subscribe" class="lktags-btn-notice"></div>

            </form>
        </div>

        <?php 
}

function lktags_notice_rate()
{
    if ( !PAnD::is_admin_notice_active( 'lktags-rating-120' ) ) {
        return;
    }
    ?>
    
            <div data-dismissible="lktags-rating-120" class="notice lktags-notice notice-success is-dismissible">
                <p class="lktags-p"><?php 
    $rating_url = "https://wordpress.org/support/plugin/bulk-image-alt-text-with-yoast/reviews/?rate=5#new-post";
    $show_support = sprintf( wp_kses( __( 'Show support for Bialty - Bulk Image Alt Text (Alt tag, Alt Attribute) with Yoast SEO + WooCommerce with a 5-star rating » <a href="%s" target="_blank">Click here</a>', 'lktags' ), array(
        'a' => array(
        'href'   => array(),
        'target' => array(),
    ),
    ) ), esc_url( $rating_url ) );
    echo  $show_support ;
    ?></p>
            </div>
    <?php 
}

add_action( 'admin_init', array( 'PAnD', 'init' ) );
//add_action( 'admin_notices', 'lktags_notice_subscribe' );
add_action( 'admin_notices', 'lktags_notice_rate' );
// end free only