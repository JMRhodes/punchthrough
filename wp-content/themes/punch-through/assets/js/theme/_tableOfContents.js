/**
 *  Initialize article table of contents block
 */
const tableOfContents = (function() {

  const pub                  = {}; // public facing functions
  const tableOfContentsBlock = document.querySelector('.block__toc');
  const tableOfContentsList  = document.querySelector('.toc__list');

  pub.init = function() {
    if (tableOfContentsBlock) {
      pub._buildTableOfContentsList();
    }
  };

  pub._buildTableOfContentsList = function() {
    const articleHeadings = document.querySelector('.entry__content--article')
      .querySelectorAll('h1.toc[id], h2.toc[id], h3.toc[id], h4.toc[id], h5.toc[id], h6.toc[id]');
    let listMarkup        = '';

    if (articleHeadings.length > 0) {
      for (let i = 0; i < articleHeadings.length; i++) {
        listMarkup += '<li><a class="toc__link" href="#' + articleHeadings[i].id + '">' + articleHeadings[i].innerHTML + '</a></li>';
      }
    }

    if (listMarkup.length > 0) {
      tableOfContentsList.innerHTML = listMarkup;
      tableOfContentsBlock.classList.add('toc--active');
      pub._listenForTOCToggle();
    }
  };

  pub._listenForTOCToggle = function() {
    const tocClose = document.querySelector('.toc__close');
    const tocOpen  = document.querySelector('.toc__open');
    const tocLinks = document.querySelectorAll('.toc__link');

    tocClose.addEventListener("click", function() {
      tableOfContentsBlock.classList.remove('toc--opened');
    });

    tocOpen.addEventListener("click", function() {
      tableOfContentsBlock.classList.add('toc--opened');
    });

    if (tocLinks.length > 0) {
      for (let i = 0; i < tocLinks.length; i++) {
        tocLinks[i].addEventListener("click", function() {
          pub._noActiveLinks();
          this.classList.add('active');
        });
      }
    }
  };

  pub._noActiveLinks = function() {
    const tocLinks = document.querySelectorAll('.toc__link');

    if (tocLinks.length > 0) {
      for (let i = 0; i < tocLinks.length; i++) {
        tocLinks[i].classList.remove('active');
      }
    }
  };

  return pub;

}());
