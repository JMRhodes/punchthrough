<?php
/**
 * ACF Module: Image Content
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$headline    = ACF::getField('headline', $data);
$content     = ACF::getField('content', $data);
$button      = ACF::getField('button', $data);
$image       = ACF::getField('image', $data);
$icon        = ACF::getField('icon', $data);
$text_align  = ACF::getField('text_align', $data, 'center');
$image_align = ACF::getField('image_align', $data, 'left');
$image_type  = ACF::getField('image_type', $data, 'default');
$card_color  = ACF::getField('card_color', $data, 'light');

$prefix        = 'image-content__card';
$image_class   = "col-md-4 image-content__image image-content__image--$image_align";
$content_class = "col-md-8 $prefix $prefix--$card_color $prefix--$image_type $prefix--$text_align";
if ('background' === $image_type) {
    $image_class   = "col-lg-4 image-content__image image-content__image--$image_align";
    $content_class = "col-lg-8 $prefix $prefix--$card_color $prefix--$text_align";
}
?>

<div id="<?php echo $row_id; ?>" class="module image-content" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="image-content__row image-content__row--<?php echo $image_type; ?>">
            <?php if ($image) : ?>
                <?php
                $attachment = [
                    'background'       => '',
                    'background_color' => '#e9e9e9',
                    'background_image' => $image,
                    'background_size'  => 'cover'
                ];
                ?>
                <div class="<?php echo $image_class; ?>"
                    <?php
                    echo 'background' === $image_type ?
                        Util::getInlineStyles($attachment, 'featured--md') : ''; ?>>
                    <?php
                    echo Util::getImageHTML(
                        Media::getAttachmentByID($image),
                        'featured--md',
                        [
                            'class' => "image--$image_type"
                        ]
                    );
                    ?>
                </div>
            <?php endif; ?>
            <div class="<?php echo $content_class; ?>">
                <div class="image-content__icon image-content__icon--<?php echo $image_align; ?>">
                    <?php echo Util::getImageHTML(Media::getAttachmentByID($icon), 'full'); ?>
                </div>
                <div class="module__heading">
                    <h2 class="content-area__title hdg hdg--2 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="image-content__body entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
                <?php if ($button) : ?>
                    <div class="image-content__button">
                        <?php echo Util::getButtonHTML($button, ['class' => 'btn btn--secondary btn--sm']); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
