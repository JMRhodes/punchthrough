<?php
/**
 * ACF Module: Heading
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline = ACF::getField('headline', $data);
$tagline  = ACF::getField('tagline', $data, get_the_title());
$content  = ACF::getField('content', $data);
$button   = ACF::getField('button', $data);
?>

<div id="<?php echo $row_id; ?>" class="module heading">
    <div class="heading__featured-image">
        <div class="module__background" <?php echo Util::getInlineStyles($data, 'featured--xl', true); ?>></div>
    </div>
    <div class="container">
    <div class="heading__body">
            <div class="row">
                <div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                    <span class="heading__tagline">
                        <?php echo esc_html($tagline); ?>
                    </span>
                    <h1 class="heading__title hdg hdg--1 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h1>
                    <div class="heading__content hdg--5 hdg--normal entry__content">
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>

                    <div class="btn__group">
                        <?php
                        echo Util::getButtonHTML($button, ['class' => 'btn btn--secondary']);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

