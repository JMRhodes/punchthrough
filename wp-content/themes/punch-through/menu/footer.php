<?php
/**
 * The Footer Site navigation
 *
 * @package Punch Through
 */


wp_nav_menu(
    [
        'theme_location'  => 'footer',
        'menu_class'      => 'footer__nav',
        'container_class' => 'collapse navbar-collapse',
        'container_id'    => 'footer-menu',
        'fallback_cb'     => false,
        'depth'           => 2,
    ]
);
