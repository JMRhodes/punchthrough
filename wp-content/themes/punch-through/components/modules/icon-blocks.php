<?php
/**
 * ACF Module: Icon Blocks
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$headline = ACF::getField('headline', $data);
$content  = ACF::getField('content', $data);
$blocks   = ACF::getRowsLayout('blocks', $data);
$button   = ACF::getField('button', $data);
?>

<div id=<?php echo $row_id; ?> class="module icon-blocks" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="module__heading">
                    <h2 class="icon-blocks__title hdg hdg--2 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="module__body hdg--6">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
            </div>

            <?php
            if (! empty($blocks)) {
                echo '<div class="blocks">';
                foreach ($blocks as $block) {
                    $icon = ACF::getField('icon', $block);

                    printf(
                        '<div class="blocks__item col-md-3">
                        <div class="blocks__icon">%3$s</div>
                        <h3 class="blocks__title hdg hdg--5 hdg--blue">%1$s</h3>
                        <div class="blocks__description entry__content">%2$s</div>
                        </div>',
                        esc_html(ACF::getField('title', $block)),
                        apply_filters('the_content', ACF::getField('description', $block)),
                        Util::getImageHTML(Media::getAttachmentByID($icon), 'full')
                    );
                }
                echo '</div>';
            }
            ?>

            <div class="btn__group col-12">
                <?php
                echo Util::getButtonHTML($button);
                ?>
            </div>
        </div>
    </div>
</div>

