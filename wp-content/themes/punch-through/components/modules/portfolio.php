<?php
/**
 * ACF Module: Portfolio
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$args = [
    'post_type'      => 'portfolio',
    'posts_per_page' => 50,
    'no_found_rows'  => true
];

$query = new WP_Query($args);
?>

<div id="<?php echo $row_id; ?>" class="module content-area" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <?php
            if ($query->have_posts()) {
                $index = 0;
                while ($query->have_posts()) {
                    $query->the_post();
                    // Loads the content/archive/portfolio.php template.
                    $file = locate_template('content/archive/portfolio.php');
                    if (file_exists($file)) {
                        include($file);
                    }

                    $index++;
                }
            } else {
                // Loads the content/singular/page.php template.
                get_template_part('content/content', 'none');
            }
            ?>
        </div>
    </div>
</div>
