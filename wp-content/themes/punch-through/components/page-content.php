<?php
/**
 * Page Component: Content
 *
 * @global $data
 */

global $post;
if (empty($post->post_content)) {
    return false;
}
?>

<div class="module page-content">
    <div class="container">
        <div class="page-content__block">
            <?php
            while (have_posts()) :
                the_post();
                ?>
                <div class="entry__content entry__content--single">
                    <?php the_content(); ?>
                </div><!-- .entry__content -->
            <?php endwhile; ?>
        </div>
    </div>
</div>