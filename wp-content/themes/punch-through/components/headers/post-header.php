<?php
/**
 * Main post header component file
 *
 * @global $format
 * @global $text_color
 */

use PT\App\Fields\ACF;

$text_color  = ACF::getField('headline_color', $meta, 'blue');
$text_color  = 'hybrid' === $format ? 'blue' : $text_color;
$author_id   = get_post_field('post_author', get_the_ID());
$author_meta = get_userdata($author_id);
?>

<header class="article__header article__header--<?php echo $format; ?>">
    <?php if ('hybrid' === $format || full === $format) : ?>
        <span class="article__hybrid">
            <?php
            the_post_thumbnail('large');
            ?>
        </span>
    <?php endif; ?>
    <div class="article__header-container <?php echo 'full' === $format ? 'col-md-9' : ''; ?>">
    <span class="article__category">
        <?php
        $category = get_the_category();
        if (! empty($category[0])) {
            printf(
                '<a href="%1$s" rel="bookmark">%2$s</a>',
                esc_url(get_category_link($category[0]->term_id)),
                esc_html($category[0]->cat_name)
            );
        }
        ?>
    </span>

        <?php the_title('<h1 class="hdg hdg--1 hdg--' . $text_color . ' hdg--light article__title">', '</h1>'); ?>

        <?php if ('banner' !== $format) : ?>
            <div class="article__date-author">
                <span class="article__date"><?php echo get_the_date(); ?></span>
                <?php
                printf(
                    'By: <a href="%2$s" class="hdg--yellow" title="Posts by %1$s" rel="author">%1$s</a>',
                    esc_html($author_meta->display_name),
                    site_url('/author/' . $author_meta->user_nicename)
                );
                ?>
            </div>
        <?php endif; ?>

        <?php
        if (! empty($overview)) {
            printf(
                '<div class="article__overview hdg--%2$s">%1$s</div>',
                apply_filters('the_content', $overview),
                $text_color
            );
        }
        ?>
    </div>
</header>
