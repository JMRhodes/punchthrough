<h3><?php 
echo  __( 'SEO tools recommended by Linkedin Insight Tags:', 'lktags' ) ;
?></h3>
        
        <?php 
// free only
?>
        <div class="lktags-overlay"> 
            <div class="lktags-overlay-bg"><h2><span class="dashicons dashicons-lock"></span> <?php 
echo  __( 'Premium Feature', 'lktags' ) ;
?></h2></div>
        <?php 
// free only
?>
            
        <div class="lktags-row row-link">
		
            <div class="lktags-column col-6 col-link">
                <div class="link-box link-pro">
                    <h3><?php 
echo  __( 'Loading performance', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Get insight on how well your site loads with actionable recommendations on how to optimize it.', 'lktags' ) ;
?></p>
                    
                    <?php 
// free only
?>
                        <a href="#" class="link-btn"><?php 
echo  __( 'Analyse my site', 'lktags' ) ;
?></a>
                    <?php 
// free only
?>
                    
                </div>
            </div>
            
            <div class="lktags-column col-6 col-link">
                <div class="link-box link-pro">
                    <h3><?php 
echo  __( 'Google Ranking', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Analyse your Google ranking with keywords, traffic and more (REGISTER TO GET 10 FREE REQUESTS)', 'lktags' ) ;
?></p>
                    
                    <?php 
// free only
?>
                        <a href="#" class="link-btn"><?php 
echo  __( 'My Google Ranking', 'lktags' ) ;
?></a>
                    <?php 
// free only
?>
                    
                </div>
            </div>
        
        </div>
            
        <div class="lktags-row">
		
            <div class="lktags-column col-6 col-link">
                <div class="link-box link-pro">
                    <h3><?php 
echo  __( 'Dead links', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Dead links, 404 errors, 503 errors, ... all bad links that could hurt your website ranking.', 'lktags' ) ;
?></p>
                    
                    <?php 
// free only
?>
                        <a href="#" class="link-btn"><?php 
echo  __( 'Check my links', 'lktags' ) ;
?></a>
                    <?php 
// free only
?>
                    
                </div>
            </div>
            
            <div class="lktags-column col-6 col-link">
                <div class="link-box link-pro">
                    <h3><?php 
echo  __( 'SEO Score', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Get a simple and professional-quality SEO audit of your website for FREE', 'lktags' ) ;
?></p>
                    
                    <?php 
// free only
?>
                        <a href="#" class="link-btn"><?php 
echo  __( 'My SEO profile', 'lktags' ) ;
?></a>
                    <?php 
// free only
?>
                    
                </div>
            </div>
        
        </div>
            
        <?php 
// free only
?>
            </div>
        <?php 
// free only
?>
            
        <div class="lktags-row">
		
            <div class="lktags-column col-6 col-link">
                <div class="link-box">
                    <h3><?php 
echo  __( 'Index Your Website (Google)', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Submit your website to Google Search Console and enjoy the real benefit of Better Robots.txt', 'lktags' ) ;
?></p>
                    <a href="https://www.google.com/webmasters/tools/home" class="link-btn"><?php 
echo  __( 'Google Search Console', 'lktags' ) ;
?></a>
                </div>
            </div>
            
            <div class="lktags-column col-6 col-link">
                <div class="link-box">
                    <h3><?php 
echo  __( 'Index your Website (Bing)', 'lktags' ) ;
?></h3>
                    <p><?php 
echo  __( 'Submit your website to Bing Webmaster tool and enjoy the real benefit of Better Robots.txt', 'lktags' ) ;
?></p>
                    <a href="https://www.bing.com/toolbox/webmaster" class="link-btn"><?php 
echo  __( 'Bing Webmaster tool', 'lktags' ) ;
?></a>
                </div>
            </div>
        
        </div>