/**
 * Custom JS Scripts
 *
 * @package Punch Through
 */

(function() {

  // Initialize Javascript modules here.
  deferImagesLoaded.init();
  parallaxBackground.init();
  mobileMenu.init();
  blockquotesSlider.init();
  tableOfContents.init();

})();
