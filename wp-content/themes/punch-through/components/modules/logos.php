<?php
/**
 * ACF Module: Logos
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$headline = ACF::getField('headline', $data);
$content  = ACF::getField('content', $data);
$logos    = ACF::getField('logos', $data);
$columns  = ACF::getField('columns', $data);
$layout   = ACF::getField('layout', $data);
?>

<div id="<?php echo $row_id; ?>" class="module logos" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="module__heading">
                    <h2 class="logos__title hdg hdg--2 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="module__body entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
                <?php if (! empty($logos)) : ?>
                    <div class="logos__grid">
                        <?php
                        foreach ($logos as $logo) {
                            $attachment = Media::getAttachmentByID($logo);
                            printf(
                                '<div class="logos__item col-6 %1$s">%2$s</div>',
                                $columns,
                                Util::getImageHTML($attachment, 'full')
                            );
                        }
                        ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
