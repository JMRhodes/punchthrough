<?php
/**
 * @phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
 */

namespace PT\App\Menus;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class MenuWalker
 * A custom menu walker for the Foundation menu structure.
 *
 * @package PT\App
 */
class PrimaryMenuWalker implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_filter('nav_menu_link_attributes', [$this, 'navMenuLinkAttributes'], 10, 4);
    }

    /**
     * Filter nav menu links to add bootstrap attributes to parent menu items
     *
     * @param $atts
     * @param $item
     * @param $args
     * @param $depth
     *
     * @return mixed $atts
     */
    public function navMenuLinkAttributes($atts, $item, $args, $depth)
    {
        // Only affect the menu placed in the 'primary' theme location
        if ('primary' !== $args->theme_location) {
            return $atts;
        }

        if (strpos($atts['href'], 'calendly.com') !== false) {
            $atts['onclick'] = 'Calendly.initPopupWidget({url:\'' . $atts['href'] . '\'});return false;';
            $atts['href']    = '#';
        }

        return $atts;
    }
}
