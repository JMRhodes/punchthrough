<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Punch Through
 */

get_header(); ?>

    <div class="container">
        <div class="row">
            <?php
            // include our archive header and links
            do_action('punch-through/archive/header');

            if (have_posts()) {
                global $wp_query;
                foreach ($wp_query->posts as $index => $post) {
                    the_post();
                    // Loads the content/archive/content.php template.
                    $file = locate_template('content/archive/content.php');
                    if (file_exists($file)) {
                        include($file);
                    }
                }
            } else {
                // Loads the content/singular/page.php template.
                get_template_part('content/content', 'none');
            }
            ?>
        </div>
    </div>

<?php get_footer();
