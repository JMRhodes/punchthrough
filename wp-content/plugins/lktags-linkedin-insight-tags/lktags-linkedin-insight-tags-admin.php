<?php

require __DIR__ . '/vendor/persist-admin-notices-dismissal/persist-admin-notices-dismissal.php';
add_action( 'admin_init', array( 'PAnD', 'init' ) );
class lktags_settings
{
    function __construct()
    {
        // stuff to do when the plugin is loaded
        add_action( 'admin_menu', array( &$this, 'lktags_admin_menu' ) );
        function lktags_styles()
        {
            wp_register_style(
                'lktags_admin-styles',
                plugin_dir_url( __FILE__ ) . 'assets/lktags-styles-admin.css',
                array(),
                filemtime( plugin_dir_path( __FILE__ ) . 'assets/lktags-styles-admin.css' )
            );
            wp_enqueue_style( 'lktags_admin-styles' );
            wp_register_script(
                'lktags_admin-script',
                plugin_dir_url( __FILE__ ) . 'assets/lktags-script-admin.js',
                array(),
                filemtime( plugin_dir_path( __FILE__ ) . 'assets/lktags-script-admin.js' )
            );
            wp_enqueue_script( 'lktags_admin-script' );
        }
        
        add_action( 'admin_enqueue_scripts', 'lktags_styles' );
    }
    
    function lktags_admin_menu()
    {
        add_options_page(
            'Linkedin Insight Tags Settings',
            'Linkedin Insight Tag',
            'manage_options',
            'lktags',
            array( &$this, 'lktags_settings_page' )
        );
    }
    
    // end function
    function lktags_settings_page()
    {
        global  $lktags ;
        $lktags_options = $lktags->lktags_options();
        //set active class for navigation tabs
        if ( isset( $_GET['tab'] ) ) {
            $active_tab = $_GET['tab'];
        }
        // end if
        $active_tab = ( isset( $_GET['tab'] ) ? $_GET['tab'] : 'lktags-settings' );
        // end active class
        
        if ( isset( $_POST['update'] ) ) {
            // check if user is authorised
            if ( function_exists( 'current_user_can' ) && !current_user_can( 'manage_options' ) ) {
                die( 'Sorry, not allowed...' );
            }
            check_admin_referer( 'lktags_settings' );
            ( isset( $_POST['enable_lktags'] ) ? $lktags_options['enable_lktags'] = $_POST['enable_lktags'] : ($lktags_options['enable_lktags'] = false) );
            $lktags_options['lktags_id'] = sanitize_text_field( $_POST['lktags_id'] );
            // tag id
            $lktags_safe = array(
                "lktags_event_yes",
                "lktags_event_no",
                "lktags_woo_yes",
                "lktags_woo_no",
                "lktags-bigta",
                "lktags-mobilook",
                "lktags-vidseo",
                "boost-alt",
                "boost-robot",
                "lktags_remove_settings"
            );
            // remove settings on plugin deactivation
            $lktags_remove = sanitize_text_field( $_POST['lktags_remove_settings'] );
            $lktags_options['lktags_remove_settings'] = ( isset( $_POST['lktags_remove_settings'] ) && in_array( $lktags_remove, $lktags_safe ) ? $lktags_remove : false );
            // boost robots.txt notice
            $lktags_robot = sanitize_text_field( $_POST['boost-robot'] );
            $lktags_options['boost-robot'] = ( isset( $_POST['boost-robot'] ) && in_array( $lktags_robot, $lktags_safe ) ? $lktags_robot : false );
            // boost alt text notice
            $lktags_alt = sanitize_text_field( $_POST['boost-alt'] );
            $lktags_options['boost-alt'] = ( isset( $_POST['boost-alt'] ) && in_array( $lktags_alt, $lktags_safe ) ? $lktags_alt : false );
            // mobilook notice
            $lktags_mobilook = sanitize_text_field( $_POST['lktags-mobilook'] );
            $lktags_options['lktags-mobilook'] = ( isset( $_POST['lktags-mobilook'] ) && in_array( $lktags_mobilook, $lktags_safe ) ? $lktags_mobilook : false );
            // install bigta notice
            $lktags_bigta = sanitize_text_field( $_POST['lktags-bigta'] );
            $lktags_options['lktags-bigta'] = ( isset( $_POST['lktags-bigta'] ) && in_array( $lktags_bigta, $lktags_safe ) ? $lktags_bigta : false );
            // install vidseo notice
            $lktags_vidseo = sanitize_text_field( $_POST['lktags-vidseo'] );
            $lktags_options['lktags-vidseo'] = ( isset( $_POST['lktags-vidseo'] ) && in_array( $lktags_vidseo, $lktags_safe ) ? $lktags_vidseo : false );
            update_option( 'lktags', $lktags_options );
            // update options
            echo  '<div class="notice notice-success is-dismissible"><p><strong>' . esc_html__( 'Settings saved.', 'lktags-linkedin-insight-tags' ) . '</strong></p></div>' ;
        }
        
        // purchase notification
        $purchase_url = "options-general.php?page=lktags-pricing";
        $get_pro = sprintf( wp_kses( __( '<a href="%s">Get Pro version</a> to enable', 'lktags-linkedin-insight-tags' ), array(
            'a' => array(
            'href'   => array(),
            'target' => array(),
        ),
        ) ), esc_url( $purchase_url ) );
        ?>

    <div class="wrap lktags-containter">

        <h2><span class="dashicons dashicons-media-text" style="margin-top: 6px; font-size: 24px;"></span> Lktags - Linkedin Insight Tags
            <?php 
        echo  esc_html__( 'Settings', 'lktags-linkedin-insight-tags' ) ;
        ?>
        </h2>

        <h2 class="nav-tab-wrapper">
            <a href="<?php 
        echo  esc_url( '?page=lktags&tab=lktags-settings' ) ;
        ?>" class="nav-tab <?php 
        echo  ( $active_tab == 'lktags-settings' ? 'nav-tab-active' : '' ) ;
        ?>">Settings</a>
            <a href="<?php 
        echo  esc_url( '?page=lktags&tab=lktags-faq' ) ;
        ?>" class="nav-tab <?php 
        echo  ( $active_tab == 'lktags-faq' ? 'nav-tab-active' : '' ) ;
        ?>">FAQ</a>
        </h2>

        <?php 
        
        if ( $active_tab == 'lktags-settings' ) {
            ?>

        <!-- start main settings column -->
        <div class="lktags-row">
            <div class="lktags-column col-9">
                <div class="lktags-main">
                    <form method="post">

                        <?php 
            if ( function_exists( 'wp_nonce_field' ) ) {
                wp_nonce_field( 'lktags_settings' );
            }
            ?>
                        
                        <br />

                        <div class="lktags-note">
                            <h3><?php 
            echo  esc_html__( 'Linkedin Insight tag, how does it work?', 'lktags-linkedin-insight-tags' ) ;
            ?></h3>
                            <p><?php 
            echo  sprintf( wp_kses( __( 'LinkedIn Insight tag (or pixel) allows you to measure the activity of users on your website and track conversion to help you understand which campaign or ad is driving the most conversions. You can also build audiences using the tag, such as people who visited a specific page on your site, or build an audience of people who converted in the past to create a remarketing ad for them. Don\'t forget to check on your pages to find META BOX feature under WordPress post editor (for Event pixel). If you have any doubt, please refer to <a href="%s" target="_blank">Linkedin documentation</a>. Enjoy.', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( 'https://www.linkedin.com/help/lms/answer/65521/the-linkedin-insight-tag-frequently-asked-questions?lang=en' ) ) ;
            ?></p>
                        </div>
                        
                        
                        <h2><?php 
            echo  esc_html__( 'LINKEDIN INSIGHT TAGS', 'lktags-linkedin-insight-tags' ) ;
            ?></h2>

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  esc_html__( 'Enable LinkedIn Insight Tag', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>

                            <div class="lktags-column col-8">
                                <label class="lktags-switch">
                                    <input type="checkbox" id="enable_lktags" name="enable_lktags"
                                    <?php 
            if ( isset( $lktags_options['enable_lktags'] ) && !empty($lktags_options['enable_lktags']) ) {
                echo  'checked="checked"' ;
            }
            ?> />
                                    <span class="lktags-slider lktags-round"></span>
                                </label>
                                &nbsp;
                                <span><?php 
            echo  esc_html__( 'This feature will add the Linkedin base code to all pages', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>

                        </div>

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  esc_html__( 'Enter LinkedIn Insight TAG ID', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                                &nbsp;
                                <div class="lktags-tooltip">
                                    <span class="dashicons dashicons-editor-help"></span>
                                    <span class="lktags-tooltiptext"><?php 
            echo  __( 'Please refer to FAQ section : How to find your TAG ID - Please do NOT enter anything else BUT your TAG ID', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                                </div>
                            </div>

                            <div class="lktags-column col-8">
                                <input type="text" name="lktags_id" id="lktags_id" class="lktags-field" value="<?php 
            if ( isset( $lktags_options['lktags_id'] ) && !empty($lktags_options['lktags_id']) ) {
                echo  stripslashes( $lktags_options['lktags_id'] ) ;
            }
            ?>" placeholder="1234567890">

                                <?php 
            
            if ( isset( $lktags_options['enable_lktags'] ) && !empty($lktags_options['enable_lktags']) && empty($lktags_options['lktags_id']) ) {
                ?>

                                    <div class="lktags-alert lktags-warning"><span class="closebtn">&times;</span><?php 
                echo  __( 'It seems you enabled LinkedIn Insight tag above but forgot to enter TAG ID. Please make sure to enter TAG ID otherwise it won\'t work.', 'lktags-linkedin-insight-tags' ) ;
                ?></div>

                                <?php 
            }
            
            ?>
                            </div>

                        </div>

                        <h2><?php 
            echo  esc_html__( 'LINKEDIN INSIGHT TAG ON WOOCOMMERCE', 'lktags-linkedin-insight-tags' ) ;
            ?></h2>
                        
                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  esc_html__( 'Enable Linkedin Insight Tag on your store', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>

                            <div class="lktags-column col-8">

                            <?php 
            // free only
            ?>

                                    <div class="lktags-switch-radio">
                                        <input type="radio" id="lktags_woo_btn1" name="lktags_woo" value="" disabled />
                                        <label for="lktags_woo_btn1"><?php 
            echo  esc_html__( 'YES', 'lktags-linkedin-insight-tags' ) ;
            ?></label>
                                        
                                        <input type="radio" id="lktags_woo_btn2" name="lktags_woo" value="" checked />
                                        <label for="lktags_woo_btn2"><?php 
            echo  esc_html__( 'NO', 'lktags-linkedin-insight-tags' ) ;
            ?></label>
                                        &nbsp;
                                        <div class="lktags-tooltip">
                                            <span class="dashicons dashicons-editor-help"></span>
                                            <span class="lktags-tooltiptext"><?php 
            echo  sprintf( wp_kses( __( 'This feature will add a "addtocart" event on your <a href="%s" target="_blank">CART PAGE</a>, just after your base code', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( plugin_dir_url( __FILE__ ) . '/assets/imgs/code.jpg' ) ) ;
            ?></span>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="lktags-alert lktags-info">
                                        <span class="closebtn">&times;</span> 
                                        <?php 
            echo  $get_pro . " " . __( 'Lktags on Woocommerce product pages.', 'lktags-linkedin-insight-tags' ) ;
            ?>
                                    </div>

                                <?php 
            ?>

                            </div>

                        </div>

                        <?php 
            // free only
            ?>

                            <div class="lktags-alert lktags-info"><span class="closebtn">&times;</span><?php 
            echo  $get_pro . sprintf( wp_kses( __( ' specific "Event pixels", which are managed with a &nbsp;<a href="%s" target="_blank">META BOX feature</a>', 'pctag' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( plugin_dir_url( __FILE__ ) . '/assets/imgs/meta-box.png' ) ) ;
            ?></div>

                        <?php 
            ?>

                        <br />                     

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  esc_html__( 'Delete Settings', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>

                            <div class="lktags-column col-8">
                                <label class="lktags-switch">
					                <input type="checkbox" id="lktags_remove_settings" name="lktags_remove_settings" value="lktags_remove_settings"
					                <?php 
            if ( $lktags_options['lktags_remove_settings'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                                    <span class="lktags-slider lktags-round"></span>
				                </label>
                                &nbsp;
                                <span><?php 
            echo  esc_html__( 'Checking this box will remove all settings when you deactivate plugin.', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>

                        </div>

                        <div class="lktags-note border" style="margin: 18px 0;"><p>😎 <i><?php 
            echo  sprintf(
                wp_kses( __( 'NEW - Get INSANE TRAFFIC with your website listed on <a href="%s" target="_blank">Baidu Webmaster Tools</a> for <a href="%s" target="_blank">Baidu.com</a>, the second largest search engine in the WORLD and the most used in China (more than 700 millions users).  <a href="%2s" target="_blank" class="note-link">SEE MORE ➤</a>', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
                'class'  => array(),
            ),
            ) ),
                esc_url( "https://ziyuan.baidu.com/" ),
                esc_url( "https://baidu.com/" ),
                esc_url( "https://better-robots.com/baidu-webmaster-tools/" )
            ) ;
            ?></i></p></div>

                        <hr>

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  __( 'Boost your ranking on Search engines', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>
                            
                            <div class="lktags-column col-8">
                                
                            <label class="lktags-switch lktags-boost-robot-label">
                                <input type="checkbox" id="boost-robot" name="boost-robot" value="boost-robot" <?php 
            if ( $lktags_options['boost-robot'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                                <span class="lktags-slider lktags-round"></span>
                            </label>

                                &nbsp; <span><?php 
            echo  __( 'Optimize site\'s crawlability with an optimized robots.txt', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                                
                                <div class="lktags-boost-robot" <?php 
            
            if ( isset( $lktags_options['boost-robot'] ) && !empty($lktags_options['boost-robot']) ) {
                echo  'style="display: inline;"' ;
            } else {
                echo  'style="display: none;"' ;
            }
            
            ?>>

                                <div class="lktags-alert lktags-success" style="margin-top: 10px;"><?php 
            echo  sprintf( wp_kses( __( 'Click <a href="%s" target="_blank">HERE</a> to Install <a href="%2s" target="_blank">Better Robots.txt plugin</a> to boost your robots.txt', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( "https://wordpress.org/plugins/better-robots-txt/" ), esc_url( "https://wordpress.org/plugins/better-robots-txt/" ) ) ;
            ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  __( 'Boost your Alt texts', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>
                            
                            <div class="lktags-column col-8">
                                
                            <label class="lktags-switch lktags-boost-alt-label">
                                <input type="checkbox" id="boost-alt" name="boost-alt" value="boost-alt" <?php 
            if ( $lktags_options['boost-alt'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                                <span class="lktags-slider lktags-round"></span>
                            </label>

                                &nbsp; <span><?php 
            echo  __( 'Boost your ranking with optimized Alt tags', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                                
                                <div class="lktags-boost-alt" <?php 
            
            if ( isset( $lktags_options['boost-alt'] ) && !empty($lktags_options['boost-alt']) ) {
                echo  'style="display: inline;"' ;
            } else {
                echo  'style="display: none;"' ;
            }
            
            ?>>

                                    <div class="lktags-alert lktags-success" style="margin-top: 10px;"><?php 
            echo  sprintf( wp_kses( __( 'Click <a href="%s" target="_blank">HERE</a> to Install <a href="%2s" target="_blank">BIALTY Wordpress plugin</a> & auto-optimize all your alt texts for FREE', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( "https://wordpress.org/plugins/bulk-image-alt-text-with-yoast/" ), esc_url( "https://wordpress.org/plugins/bulk-image-alt-text-with-yoast/" ) ) ;
            ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>

                        <div class="lktags-row">

                            <div class="lktags-column col-4">
                                <span class="lktags-label"><?php 
            echo  __( 'Mobile-Friendly & responsive design', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                            </div>
                            
                            <div class="lktags-column col-8">
                                
                            <label class="lktags-switch lktags-mobi-label">
                                <input type="checkbox" id="lktags-mobilook" name="lktags-mobilook" value="lktags-mobilook" <?php 
            if ( $lktags_options['lktags-mobilook'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                                <span class="lktags-slider lktags-round"></span>
                            </label>

                                &nbsp; <span><?php 
            echo  __( 'Get dynamic mobile previews of your pages/posts/products + Facebook debugger', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                                
                                <div class="lktags-mobi" <?php 
            
            if ( isset( $lktags_options['lktags-mobilook'] ) && !empty($lktags_options['lktags-mobilook']) ) {
                echo  'style="display: inline;"' ;
            } else {
                echo  'style="display: none;"' ;
            }
            
            ?>>

                                    <div class="lktags-alert lktags-success" style="margin-top: 10px;"><?php 
            echo  sprintf( wp_kses( __( 'Click <a href="%s" target="_blank">HERE</a> to Install <a href="%2s" target="_blank">Mobilook</a> and test your website on Dualscreen format (Galaxy fold)', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( "https://wordpress.org/plugins/mobilook/" ), esc_url( "https://wordpress.org/plugins/mobilook/" ) ) ;
            ?>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
			
                        <div class="lktags-row">

                        <div class="lktags-column col-4">
                            <span class="lktags-label"><?php 
            echo  __( 'Boost your image title attribute', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                        </div>

                        <div class="lktags-column col-8">

                        <label class="lktags-switch lktags-bigta-label">
                            <input type="checkbox" id="lktags-bigta" name="lktags-bigta" value="lktags-bigta" <?php 
            if ( $lktags_options['lktags-bigta'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                            <span class="lktags-slider lktags-round"></span>
                        </label>

                        &nbsp; <span><?php 
            echo  __( 'Optimize all your image title attributes for UX & search engines performance', 'lktags-linkedin-insight-tags' ) ;
            ?></span>

                            <div class="lktags-bigta" <?php 
            
            if ( isset( $lktags_options['lktags-bigta'] ) && !empty($lktags_options['lktags-bigta']) ) {
                echo  'style="display: inline;"' ;
            } else {
                echo  'style="display: none;"' ;
            }
            
            ?>>

                                <div class="lktags-alert lktags-success" style="margin-top: 10px;"><?php 
            echo  sprintf( wp_kses( __( 'Click <a href="%s" target="_blank">HERE</a> to Install <a href="%2s" target="_blank">BIGTA</a> Wordpress plugin & auto-optimize all your image title attributes for FREE', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( "https://wordpress.org/plugins/bulk-image-title-attribute/" ), esc_url( "https://wordpress.org/plugins/bulk-image-title-attribute/" ) ) ;
            ?>
                                </div>
                            </div>
                        </div>

                        </div>

                        <div class="lktags-row">

                        <div class="lktags-column col-4">
                            <span class="lktags-label"><?php 
            echo  __( 'Looking for FREE unlimited content for SEO?', 'lktags-linkedin-insight-tags' ) ;
            ?></span>
                        </div>

                        <div class="lktags-column col-8">

                        <label class="lktags-switch lktags-vidseo-label">
                            <input type="checkbox" id="lktags-vidseo" name="lktags-vidseo" value="lktags-vidseo" <?php 
            if ( $lktags_options['lktags-vidseo'] ) {
                echo  'checked="checked"' ;
            }
            ?> />
                            <span class="lktags-slider lktags-round"></span>
                        </label>

                        &nbsp; <span><?php 
            echo  __( 'Get access to billions of non-indexed content with Video transcripts (Youtube)', 'lktags-linkedin-insight-tags' ) ;
            ?></span>

                            <div class="lktags-vidseo" <?php 
            
            if ( isset( $lktags_options['lktags-vidseo'] ) && !empty($lktags_options['lktags-vidseo']) ) {
                echo  'style="display: inline;"' ;
            } else {
                echo  'style="display: none;"' ;
            }
            
            ?>>

                                <div class="lktags-alert lktags-success" style="margin-top: 10px;"><?php 
            echo  sprintf( wp_kses( __( 'Click <a href="%s" target="_blank">HERE</a> to learn more about <a href="%2s" target="_blank">VidSEO</a> Wordpress plugin & how to skyrocket your SEO', 'lktags-linkedin-insight-tags' ), array(
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
                'a' => array(
                'href'   => array(),
                'target' => array(),
            ),
            ) ), esc_url( "https://wordpress.org/plugins/vidseo/" ), esc_url( "https://wordpress.org/plugins/vidseo/" ) ) ;
            ?>
                                </div>
                            </div>
                        </div>

                        </div>

                        <p class="submit"><input type="submit" name="update" class="button-primary" value="<?php 
            echo  esc_html__( 'Save Changes', 'lktags-linkedin-insight-tags' ) ;
            ?>" /></p>
                    </form>

                    <div class="lktags-note"><p><?php 
            echo  __( "Note: once the codes are added, make sure to clear your cache. Then, you must add the domain for approval. To do this, go to the Insight Tag page, and on the right side, you must add your domain. WWW is not required. Once users enter your website, the tag will recognize and start running. At this point, you will see that LinkedIn has confirmed your domain.", 'lktags-linkedin-insight-tags' ) ;
            ?></p></div>
        


                </div>
                <!-- end lktags-main -->
            </div>
            <!-- end main settings lktags-column col-8 -->

            <?php 
            include dirname( __FILE__ ) . '/inc/sidebar.php';
        }
        
        if ( $active_tab == 'lktags-faq' ) {
            include dirname( __FILE__ ) . '/inc/faq.php';
        }
        ?>

        </div>
    
    </div>

        <?php 
    }

}
// end class
$lktags_settings = new lktags_settings();