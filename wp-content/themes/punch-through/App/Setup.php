<?php

namespace PT\App;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Setup
 *
 * @package Punch Through\App
 */
class Setup implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('wp_head', [$this, 'outputSiteFavicons']);
        add_action('init', [$this, 'registerMenus']);
    }

    /**
     * Output our favicon markup in the header
     */
    public function outputSiteFavicons()
    {
        $assets_dir = PT_THEME_PATH_URL . 'assets/images/favicons/';
        ?>
        <link rel="shortcut icon" href="<?php echo $assets_dir; ?>favicon.ico" type="image/x-icon"/>
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $assets_dir; ?>apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $assets_dir; ?>apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $assets_dir; ?>apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $assets_dir; ?>apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $assets_dir; ?>apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $assets_dir; ?>apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $assets_dir; ?>apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $assets_dir; ?>apple-touch-icon-152x152.png">
        <link rel="icon" type="image/png" href="<?php echo $assets_dir; ?>favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo $assets_dir; ?>favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo $assets_dir; ?>favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo $assets_dir; ?>android-chrome-192x192.png" sizes="192x192">
        <?php
    }

    /**
     * Registers nav menu locations.
     */
    public function registerMenus()
    {
        register_nav_menu('primary', __('Primary', 'punch-through'));
        register_nav_menu('footer', __('Footer', 'punch-through'));
    }
}
