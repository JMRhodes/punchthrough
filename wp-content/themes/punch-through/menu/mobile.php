<?php
/**
 * The Primary Site navigation mobile view
 *
 * @package Punch Through
 */

?>

<div class="mobile-menu">
    <?php
    wp_nav_menu(
        [
            'theme_location'  => 'primary',
            'menu_class'      => 'mobile-menu__nav',
            'container_id'    => 'primary-menu',
            'fallback_cb'     => false,
            'depth'           => 1
        ]
    );
    ?>
</div>