// Testimonial Styles

.testimonial {

  &__block {
    @extend %flex-row;
    position: relative;
    overflow: hidden;
    align-items: flex-end;

    &:after {
      z-index: 3;
      content: '';
      position: absolute;
      bottom: 0;
      right: 0;
      width: 0;
      height: 0;
      border-top: _calc-rem(40px) solid transparent;
      border-right: _calc-rem(25px) solid $white;

      @include media-breakpoint-up('md') {
        border-top: _calc-rem(115px) solid transparent;
        border-right: _calc-rem(70px) solid $white;
      }
    }
  }

  &__image {
    padding: 0;
    position: relative;
    z-index: 2;
    display: none;

    @include media-breakpoint-up('lg') {
      display: block;
    }

    img {
      width: 100%;
      height: auto;
    }
  }

  &__body {
    position: relative;
    z-index: 2;
  }

  &__content {
    text-align: center;
    background: $bianca;
    position: relative;
    padding: ($spacer * 2.5) ($spacer * 3);

    &:before {
      content: '';
      position: absolute;
      top: 0;
      bottom: 0;
      right: 0;
      left: -100%;
      background: $bianca;
      z-index: 0;
    }

    @include media-breakpoint-up('lg') {
      padding: ($spacer * 2.5) ($spacer * 5) ($spacer * 4) 0;
    }
  }

  &__logo {
    max-height: _calc-rem(42px);
    width: auto;
    display: inline-block;
    margin-bottom: ($spacer * 1.5);
  }

  &__name {
    color: $cerulean;
  }
}
