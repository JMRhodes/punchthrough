<?php
/**
 * Post Format: Banner
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$background = [
    'background_color' => '#0E3957',
    'background_size'  => 'cover'
];

if (has_post_thumbnail()) {
    $meta                           = ACF::getPostMeta(get_the_ID());
    $thumbnail_id                   = ACF::getField('_thumbnail_id', $meta);
    $background['background_image'] = $thumbnail_id;
}

$text_color      = ACF::getField('headline_color', $meta, 'white');
$overlay_opacity = ACF::getField('overlay_opacity', $meta, '80');
$overview        = ACF::getField('overview', $meta, '80');
?>

<style type="text/css">
    .post-header .module__background:before {
        content: '';
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        background: #0E3957;
        opacity: .<?= (int)$overlay_opacity ?>;
    }
</style>

<div class="post-header post-header--banner">
    <div class="module__background" <?php echo Util::getInlineStyles($background, 'featured--xl', true); ?>></div>
    <div class="container">
        <div class="module__content">
            <?php
            $file = locate_template("components/headers/post-header.php");
            if (file_exists($file)) {
                include($file);
            }
            ?>
        </div>
    </div>
</div>
