<?php
/**
 * ACF Module: Gallery
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Media;
use PT\App\Fields\Util;

$images = ACF::getField('images', $data, []);

if (! $images) {
    return;
}
?>

<div id="<?php echo $row_id; ?>" class="module gallery" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <?php
            foreach ((array)$images as $image) {
                $attachment = Media::getAttachmentByID($image);
                $image_meta = ACF::getPostMeta($image);
                $headline   = ACF::getField('headline', $image_meta);
                $content    = ACF::getField('content', $image_meta);
                $url        = ACF::getField('url', $image_meta);

                printf(
                    '<div class="gallery__item col-sm-6 col-md-4">
                        %4$s
                        %1$s 
                        <div class="gallery__item-content">
                            <h4 class="hdg hdg--5 hdg--blue hdg--normal">%2$s</h4>
                            %3$s
                        </div>
                        %5$s
                    </div>',
                    Util::getImageHTML($attachment, 'featured--md'),
                    $headline,
                    $content,
                    (! empty($url) ? '<a href="' . esc_url($url) . '" target="_blank">' : ''),
                    (! empty($url) ? '</a>' : '')
                );
            }
            ?>
        </div>
    </div>
</div>
