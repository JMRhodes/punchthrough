<?php
/**
 * The Primary Site navigation
 *
 * @package Punch Through
 */

wp_nav_menu(
    [
        'theme_location'  => 'primary',
        'menu_class'      => 'navbar__nav',
        'container_id'    => 'primary-menu',
        'fallback_cb'     => false,
        'depth'           => 1
    ]
);
