<?php
/**
 * ACF Module: Columns
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$columns = ACF::getRowsLayout('columns', $data);

if (empty($columns)) {
    return false;
}
?>

<div id="<?php echo $row_id; ?>" class="module content-columns" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <?php
            foreach ($columns as $column) :
                $attachment = Media::getAttachmentByID(ACF::getField('image', $column));
                $headline = ACF::getField('headline', $column);
                $content = ACF::getField('content', $column);
                $column = ACF::getField('width', $column, 'half');
                ?>
                <div class="col-md-<?php echo 'half' === $column ? '6' : '12'; ?>">
                    <div class="module__image module__image--hide module__image--margin-bottom">
                        <?php echo Util::getImageHTML($attachment, 'featured--md'); ?>
                    </div>
                    <div class="module__heading">
                        <h2 class="content-area__title hdg hdg--2 hdg--light hdg--blue">
                            <?php echo esc_html($headline); ?>
                        </h2>
                    </div>
                    <div class="module__body entry__content text--left">
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
