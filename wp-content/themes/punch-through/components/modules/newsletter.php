<?php
/**
 * ACF Module: Newsletter
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$block    = ACF::getField('block', $data);
$headline = ACF::getField('headline', $data);
$content  = ACF::getField('description', $data);
$form_id  = ACF::getField('form_id', $data);

if (! $form_id) {
    return false;
}
?>

<div id="<?php echo $row_id; ?>" class="module newsletter" <?php echo Util::getInlineStyles($data); ?>>
    <?php if (! $block) : ?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
    <?php endif; ?>
                <div class="newsletter__block">
                    <div class="module__heading">
                        <h2 class="banner__title hdg hdg--2 hdg--light hdg--yellow" style="color: #EFB000;">
                            <?php echo esc_html($headline); ?>
                        </h2>
                    </div>
                    <div class="module__body">
                        <?php
                        echo apply_filters('the_content', $content);
                        wpforms_display($form_id);
                        ?>
                    </div>
                </div>
    <?php if (! $block) : ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>

