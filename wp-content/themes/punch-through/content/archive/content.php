<?php
/**
 * @package Punch Through
 *
 * @global $index
 */

use PT\App\Posts\Articles;
use PT\App\Media;
use PT\App\Fields\Util;
use PT\App\Fields\Options;

$default       = Options::getSiteOption('default_featured_image');
$article_class = (0 === $index ? 'entry__article--first col-12' : 'col-md-6');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class("entry__article {$article_class}"); ?>>
    <div class="entry__item">
        <div class="entry__thumb">
            <?php
            $attachment_id = has_post_thumbnail() ? get_post_thumbnail_id(get_the_ID()) : $default;
            $attachment    = Media::getAttachmentByID($attachment_id);
            $image_size    = (0 === $index ? 'featured--lg' : 'featured--sm');

            printf(
                '<a href="%1$s" rel="bookmark">%2$s</a>',
                esc_url(get_permalink()),
                Util::getImageHTML($attachment, $image_size)
            );
            ?>
        </div>

        <span class="entry__category">
        <?php
        $category = get_the_category();
        if (! empty($category[0])) {
            printf(
                '<a href="%1$s" rel="bookmark">%2$s</a>',
                esc_url(get_category_link($category[0]->term_id)),
                esc_html($category[0]->cat_name)
            );
        }
        ?>
    </span>

        <div class="entry__body">
            <header class="entry__header">
                <?php
                $hdg = (0 === $index ? 'hdg--3 hdg--yellow' : 'hdg--4');
                the_title(
                    sprintf(
                        '<h2 class="hdg %2$s hdg--light"><a href="%1$s" rel="bookmark">',
                        esc_url(get_permalink()),
                        $hdg
                    ),
                    '</a></h1>'
                );
                ?>
            </header><!-- .entry__header -->

            <div class="entry__author">
                <?php printf('By: %1$s', get_the_author_posts_link()); ?>
            </div><!-- .entry__content -->

            <div class="entry__excerpt">
                <?php
                $characters = (0 === $index ? 450 : 250);
                echo Articles::getContentLimited($post->post_content, $characters);
                ?>
            </div><!-- .entry__content -->
        </div>
    </div>
</article><!-- #post-## -->