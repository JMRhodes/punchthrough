<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Punch Through
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('article__page'); ?>>

    <div class="entry__content entry__content--single">
        <?php the_content(); ?>
    </div><!-- .entry__content -->

</article><!-- #post-## -->
