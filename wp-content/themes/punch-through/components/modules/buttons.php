<?php
/**
 * ACF Module: Banner
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline      = ACF::getField('headline', $data);
$content       = ACF::getField('content', $data);
$buttons       = ACF::getRowsLayout('buttons', $data);
$button_layout = ACF::getField('button_layout', $data, 'inline');
?>

<div id="<?php echo $row_id; ?>" class="module buttons" <?php echo Util::getInlineStyles($data); ?>>
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <div class="module__heading">
                <h2 class="banner__title hdg hdg--2 hdg--light hdg--blue">
                    <?php echo esc_html($headline); ?>
                </h2>
            </div>
            <div class="module__body entry__content">
                <?php echo apply_filters('the_content', $content); ?>
            </div>
        </div>
        <div class="buttons__group buttons__group--<?php echo $button_layout; ?>">
            <?php
            foreach ($buttons as $button) {
                if (empty($button['button'])) {
                    continue;
                }
                $classes = 'inline' === $button_layout ? 'col-12 col-md-6 col-lg-4' : 'buttons__item--stacked';
                echo '<div class="buttons__item ' . $classes . '">';
                echo Util::getButtonHTML($button['button'], ['class' => 'btn btn--primary']);
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>
</div>

