<?php
/**
 * ACF Module: Image
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Media;
use PT\App\Fields\Util;

$image = ACF::getField('image', $data);
$notch = ACF::getField('image_notch', $data, 'show');

if (! $image) {
    return;
}
?>

<div id="<?php echo $row_id; ?>" class="module image" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="module__image module__image--<?php echo $notch; ?>">
            <?php
            $attachment = Media::getAttachmentByID($image);
            echo Util::getImageHTML($attachment, 'featured--xl');
            ?>
        </div>
    </div>
</div>
