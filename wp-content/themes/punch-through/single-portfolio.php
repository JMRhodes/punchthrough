<?php
/**
 * Single Portfolio Template
 *
 * This template displays Advanced Custom Fields
 * flexible content fields in a user-defined order.
 *
 * @package Punch Through
 */

get_header();

// hook: App/Fields/Modules/outputFlexibleModules()
do_action('pt/modules/output', get_the_ID());

get_footer();
