<?php
/**
 * ACF Module: List Columns
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$lists = ACF::getRowsLayout('lists', $data);

if (empty($lists)) {
    return false;
}
?>

<div id="<?php echo $row_id; ?>" class="module list-columns" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="list-column">
            <?php
            foreach ($lists as $column) :
                $attachment = Media::getAttachmentByID(ACF::getField('image', $column));
                $headline = ACF::getField('headline', $column);
                $list = ACF::getField('list', $column);
                $column = ACF::getField('column', $column, 'col-md-4');
                ?>
                <div class="col-12 <?php echo $column; ?>">
                    <div class="module__heading text--left">
                        <h3 class="content-area__title hdg hdg--4 hdg--light hdg--blue">
                            <?php echo esc_html($headline); ?>
                        </h3>
                    </div>
                    <div class="module__body entry__content entry__content--no-style-type text--left">
                        <?php echo apply_filters('the_content', $list); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
