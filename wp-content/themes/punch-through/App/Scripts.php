<?php

namespace PT\App;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Scripts
 *
 * @package Punch Through\App
 */
class Scripts implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueCalendlyScripts']);
        add_action('wp_enqueue_scripts', [$this, 'enqueueStyles']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminStyles']);
    }

    /**
     * Load scripts for the front end.
     */
    public function enqueueScripts()
    {

        wp_enqueue_script(
            'pt-vendor',
            get_stylesheet_directory_uri() . '/build/js/vendor.min.js',
            ['jquery'],
            THEME_VERSION,
            true
        );

        wp_enqueue_script(
            'pt-theme',
            get_stylesheet_directory_uri() . "/build/js/theme.min.js",
            ['jquery'],
            THEME_VERSION,
            true
        );

        if (is_singular() && comments_open() && get_option('thread_comments')) {
            wp_enqueue_script('comment-reply');
        }
    }

    /**
     * Load stylesheets for the front end.
     */
    public function enqueueStyles()
    {
        wp_dequeue_style('wp-block-library');

        wp_enqueue_style(
            'pt-styles',
            get_stylesheet_directory_uri() . '/build/css/theme.min.css',
            [],
            THEME_VERSION
        );
    }

    /**
     * Load stylesheets for the front end.
     */
    public function enqueueAdminStyles()
    {
        wp_enqueue_style(
            'pt-admin-styles',
            get_stylesheet_directory_uri() . '/build/css/admin.min.css',
            [],
            THEME_VERSION
        );
    }

    public function enqueueCalendlyScripts()
    {
        wp_enqueue_style(
            'pt-calendly-styles',
            'https://assets.calendly.com/assets/external/widget.css',
            [],
            THEME_VERSION
        );

        wp_enqueue_script(
            'pt-calendly-scripts',
            'https://assets.calendly.com/assets/external/widget.js',
            [],
            THEME_VERSION
        );
    }
}
