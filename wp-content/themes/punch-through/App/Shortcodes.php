<?php

namespace PT\App;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Shortcodes
 *
 * @package Punch Through\App
 */
class Shortcodes implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_shortcode('button', [$this, 'button']);
        add_shortcode('tooltip', [$this, 'tooltip']);
        add_shortcode('bio', [$this, 'authorBio']);
    }

    /**
     * Generate author Bio markup
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */
    public function authorBio($atts, $content = null)
    {
        $atts = shortcode_atts(
            [
                'user' => false
            ],
            $atts,
            'bio'
        );
        if (empty($atts['user'])) {
            return false;
        }

        $author_id   = $atts['user'];
        $author_meta = get_userdata($author_id);

        return sprintf(
            '<div class="article__meta">
                <div class="article__author">
                    <div class="article__avatar">
                        %1$s
                    </div>
                    By: <a href="%3$s" title="Posts by %2$s" rel="author">%2$s</a>
                    <span class="article__bio">%4$s</span>
                </div>
            </div>',
            get_avatar(get_the_author_meta('ID', $author_id), 164),
            esc_html($author_meta->display_name),
            site_url('/author/' . $author_meta->user_nicename),
            get_the_author_meta('description', $author_id)
        );
    }

    /**
     * Generate button markup
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */
    public function button($atts, $content = null)
    {
        $atts = shortcode_atts(
            [
                'link'    => '#',
                'target'  => '_blank',
                'classes' => 'btn',
                'style'   => 'btn-default',
                'block'   => ''
            ],
            $atts,
            'button'
        );

        $classes = $atts['classes'] . ' ' . $atts['style'];
        $classes .= (! empty($atts['block']) && 'true' === $atts['block']) ? ' btn-block' : '';

        return "<a class=\"{$classes}\" href=\"{$atts['link']}\" target=\"{$atts['target']}\">{$content}</a>";
    }

    /**
     * Bootstrap tooltip markup
     *
     * @param $atts
     * @param null $content
     *
     * @return string
     */
    public function tooltip($atts, $content = null)
    {
        $atts = shortcode_atts(
            [
                'text'      => 'NO TEXT ENTERED',
                'placement' => 'top'
            ],
            $atts,
            'tooltip'
        );

        return "<span data-toggle=\"tooltip\" data-placement=\"{$atts['placement']}\" title=\"{$atts['text']}\">" . do_shortcode($content) . "</span>";
    }
}
