<?php
/**
 * The template for displaying the footer.
 *
 * @package Punch Through
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Options;
use PT\App\Fields\Util;
use PT\App\Media;

$options    = Options::getSiteOptions();
$contact    = ACF::getField('contact', $options);
$newsletter = ACF::getField('newsletter_form', $options);
?>

<footer class="footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="footer__row">
                <div class="footer__item footer__item--logo">
                    <?php
                    $footer_logo = ACF::getField('footer_logo', $options);
                    if (! empty($footer_logo)) {
                        echo Util::getImageHTML(Media::getAttachmentByID($footer_logo, 'full'));
                    }
                    ?>
                </div>
                <div class="footer__item footer__item--menu">
                    <?php
                    // Loads the menu/footer.php template.
                    get_template_part('menu/footer');
                    ?>
                </div>
                <div class="footer__item footer__item--contact entry__content entry__content--footer">
                    <div class="footer__item-title">
                        <?php _e('Contact', 'punch-through'); ?>
                    </div>
                    <?php echo apply_filters('the_content', $contact); ?>
                </div>
                <div class="footer__item footer__item--copyright">
                    <?php
                    if ($newsletter) {
                        wpforms_display($newsletter, true, true);
                    }
                    printf(
                        '&copy %1$s %2$s. ' . __('All Rights Reserved.', 'punch-through'),
                        date('Y'),
                        get_bloginfo('name')
                    );
                    ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>