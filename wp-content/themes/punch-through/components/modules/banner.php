<?php
/**
 * ACF Module: Banner
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline   = ACF::getField('headline', $data);
$content    = ACF::getField('content', $data);
$button     = ACF::getField('button', $data);
$button_alt = ACF::getField('button_alt', $data);
?>

<div id="<?php echo $row_id; ?>" class="module banner" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1">
                <div class="module__heading">
                    <h2 class="banner__title hdg hdg--2 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="module__body entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>

                <div class="btn__group">
                    <?php
                    echo Util::getButtonHTML($button, ['class' => 'btn btn--primary btn--sm']);

                    echo Util::getButtonHTML($button_alt, ['class' => 'btn btn--secondary btn--sm']);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

