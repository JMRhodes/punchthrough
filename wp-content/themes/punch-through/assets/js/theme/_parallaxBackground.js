/**
 *  Initialize jarallax
 */
const parallaxBackground = (function() {

  const pub = {}; // public facing functions

  pub.init = function() {
    const parallaxElements = document.querySelectorAll('.jarallax');

    if (parallaxElements.length > 0) {
      jarallax(parallaxElements, {
        speed: 0.1,
        imgSize: 'none',
        onInit: function() {
          setTimeout(function() {
            for (let i = 0; i < parallaxElements.length; i++) {
              parallaxElements[i].classList.add('jarallax--loaded');
            }
          }, 500);
        }
      });
    }
  };

  return pub;

}());
