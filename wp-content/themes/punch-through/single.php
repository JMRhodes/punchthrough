<?php
/**
 * The template for displaying all single posts.
 *
 * @package Punch Through
 */

use PT\App\Fields\ACF;

get_header();

$meta   = ACF::getPostMeta(get_the_ID());
$format = ACF::getField('post_format', $meta, 'text');
$primary_class = 'full' === $format ? 'col-md-12' : 'col-md-9';

do_action('punch-through/posts/format', $format);
?>

    <div class="container">
        <div class="row">
            <div id="primary" class="<?php echo $primary_class ;?> article__page article__page--<?php echo $format; ?>">
                <?php
                while (have_posts()) {
                    the_post();
                    // Loads the content/singular/page.php template.
                    get_template_part('content/singular/' . get_post_type());
                }
                ?>
            </div><!-- /#primary -->

            <?php
            if ($format !== 'full') {
                get_sidebar();
            }
            ?>
        </div>
    </div>

<?php get_footer();
