<?php
/**
 * ACF Module: Blockquotes
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$headline   = ACF::getField('headline', $data);
$quotes     = ACF::getRowsLayout('quotes', $data);
$icon       = ACF::getField('icon', $data);
$quotations = ACF::getField('quotations', $data);
?>

<div id="<?php echo $row_id; ?>" class="module blockquotes" <?php echo Util::getInlineStyles($data); ?>>
    <svg aria-hidden="true" style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1"
         xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-right-quote" viewBox="0 0 32 32">
                <title>right-quote</title>
                <path
                    d="M0 4v12h8c0 4.41-3.586 8-8 8v4c6.617 0 12-5.383 12-12v-12h-12zM20 4v12h8c0 4.41-3.586 8-8 8v4c6.617 0 12-5.383 12-12v-12h-12z"></path>
            </symbol>
            <symbol id="icon-left-quote" viewBox="0 0 32 32">
                <title>left-quote</title>
                <path
                    d="M32 28v-12h-8c0-4.41 3.586-8 8-8v-4c-6.617 0-12 5.383-12 12v12h12zM12 28v-12h-8c0-4.41 3.586-8 8-8v-4c-6.617 0-12 5.383-12 12v12h12z"></path>
            </symbol>
        </defs>
    </svg>
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 blockquotes__row">
                <?php if ($headline) : ?>
                    <div class="module__heading">
                        <h2 class="banner__title hdg hdg--2 hdg--light hdg--blue">
                            <?php echo esc_html($headline); ?>
                        </h2>
                    </div>
                <?php endif; ?>
                <?php if (! empty($quotes)) : ?>
                    <div class="quotes">
                        <?php
                        foreach ($quotes as $item) {
                            $name   = ACF::getField('name', $item);
                            $title  = ACF::getField('title', $item);
                            $avatar = ACF::getField('avatar', $item);
                            $quote  = ACF::getField('quote', $item);

                            printf(
                                '<div class="quotes__item">
                                    <div class="quotes__container">
                                        <div class="quotes__body quotes__body--%5$s">
                                            <div class="quotes__quote quotes__quote--left">
                                                <svg class="icon icon-left-quote"><use xlink:href="#icon-left-quote"></use></svg>
                                            </div>
                                            <div class="quotes__content">%4$s</div>
                                            <div class="quotes__quote quotes__quote--right">
                                                <svg class="icon icon-right-quote"><use xlink:href="#icon-right-quote"></use></svg>
                                            </div>
                                        </div>
                                        <div class="quotes__meta">
                                            %3$s
                                            <h4 class="hdg hdg--6 hdg--light hdg--blue">%1$s</h4>
                                            <span class="quotes__title">%2$s</span>
                                        </div>
                                    </div>
                                </div>',
                                esc_html($name),
                                esc_html($title),
                                Util::getImageHTML(
                                    Media::getAttachmentByID($avatar),
                                    'featured--md',
                                    [
                                        'class' => "quotes__avatar"
                                    ]
                                ),
                                apply_filters('the_content', $quote),
                                $quotations ? 'quotes' : 'no-quotes'
                            );
                        }
                        ?>
                    </div>
                <?php endif; ?>
                <?php if ($icon) : ?>
                    <div class="quotes__icon quotes__icon--<?php echo $image_align; ?>">
                        <?php echo Util::getImageHTML(Media::getAttachmentByID($icon), 'full'); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

