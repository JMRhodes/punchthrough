<?php
/**
 * Post Format: Image
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$background = [
    'background_color' => '#0E3957',
    'background_size'  => 'cover'
];

if (has_post_thumbnail()) {
    $meta                           = ACF::getPostMeta(get_the_ID());
    $thumbnail_id                   = ACF::getField('_thumbnail_id', $meta);
    $background['background_image'] = $thumbnail_id;
}
?>

<div class="post-header post-header--text">
    <div class="container">
        <?php
        $file = locate_template("components/headers/post-header.php");
        if (file_exists($file)) {
            include($file);
        }
        ?>
    </div>
</div>
