<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>
<?php

use PT\App\Fields\Options;
use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

// get site options
$options = Options::getSiteOptions();
?>
<body <?php body_class('lang-markup'); ?>>

<?php
// Loads the menu/mobile.php template.
get_template_part('menu/mobile');
?>

<!-- skip to main content -->
<a href="#primary" class="screen-reader-text"><?php _e('Skip to Main Content', 'punch-through'); ?></a>

<header id="masthead" class="header" role="banner">
    <div class="container">
        <nav class="navbar">
            <a class="navbar__brand" href="<?php echo home_url(); ?>">
                <?php
                $site_logo = ACF::getField('site_logo', $options);
                if (! empty($site_logo)) {
                    echo Util::getImageHTML(Media::getAttachmentByID($site_logo), 'full');
                } else {
                    bloginfo('name');
                }
                ?>
            </a>

            <?php
            // Loads the menu/primary.php template.
            get_template_part('menu/primary');
            ?>

            <div class="menu-icon" data-toggle="menu">
                <span class="menu-icon__line menu-icon__line-left"></span>
                <span class="menu-icon__line"></span>
                <span class="menu-icon__line menu-icon__line-right"></span>
            </div>
        </nav>
    </div>
</header><!-- .header -->