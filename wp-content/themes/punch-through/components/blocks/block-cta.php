<?php
/**
 * CTA Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

use PT\App\Fields\Util;
use PT\App\Fields\ACF;

// Create id attribute allowing for custom "anchor" value.
$id = 'cta-' . $block['id'];
if (! empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'cta';
if (! empty($block['className'])) {
    $className .= ' ' . $block['className'];
}

// Load values and assing defaults.
$headline   = get_field('headline') ?: 'Headline';
$content    = get_field('content') ?: 'Lorem ipsum dolor sit amen.';
$button     = get_field('button') ?: [];
$background = get_field('background_color') ?: 'blue';
$layout     = get_field('layout') ?: 'stacked';
?>

<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?> cta--<?php echo $layout; ?>">
    <div class="cta__body">
        <div class="module__heading">
            <h2 class="cta__title hdg hdg--2 hdg--light hdg--blue">
                <?php echo esc_html($headline); ?>
            </h2>
        </div>
        <div class="hdg--6">
            <?php echo esc_html($content); ?>
        </div>
    </div>
    <div class="cta__button">
        <?php
        echo Util::getButtonHTML(
            $button,
            ['class' => ($background === 'blue' ? 'btn btn--primary' : 'btn btn--yellow')]
        );
        ?>
    </div>
    <style type="text/css">
        <?php
        $background_color = 'blue' === $background ? '#00ADEF' : '#0E3957';
        $heading_color = 'blue' === $background ? '#0E3957' : '#EFB000';
        printf(
            '#%1$s {background: %2$s;color: #FFFFFF;}
            #%1$s .hdg {color: %3$s;}',
            $id,
            $background_color,
            $heading_color
        );
        ?>
    </style>
</div>