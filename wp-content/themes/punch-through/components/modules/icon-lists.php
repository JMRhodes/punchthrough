<?php
/**
 * ACF Module: Icon Blocks
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$headline = ACF::getField('headline', $data);
$content  = ACF::getField('content', $data);
$list     = ACF::getRowsLayout('list', $data);
$button   = ACF::getField('button', $data);
?>

<div id="<?php echo $row_id; ?>" class="module icon-lists" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="module__heading">
            <h2 class="icon-lists__title hdg hdg--2 hdg--light hdg--blue">
                <?php echo esc_html($headline); ?>
            </h2>
        </div>
        <div class="module__body hdg--6">
            <?php echo apply_filters('the_content', $content); ?>
        </div>

        <?php
        if (! empty($list)) {
            echo '<div class="lists">';
            foreach ($list as $item) {
                $icon = ACF::getField('icon', $item);

                printf(
                    '<div class="lists__item">
                        <div class="lists__icon">%3$s</div>
                        <div class="lists__body">
                        <h3 class="lists__title hdg hdg--4 hdg--light hdg--blue">%1$s</h3>
                        <div class="lists__description entry__content">%2$s</div>
                        </div>
                        </div>',
                    esc_html(ACF::getField('title', $item)),
                    apply_filters('the_content', ACF::getField('description', $item)),
                    Util::getImageHTML(Media::getAttachmentByID($icon), 'full')
                );
            }
            echo '</div>';
        }
        ?>
    </div>
</div>

