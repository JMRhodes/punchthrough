<?php

namespace PT\App\Blocks;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Scripts
 *
 * @package Punch Through\App\Blocks
 */
class CTA implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('acf/init', [$this, 'registerCTABlock']);
    }

    /**
     * Load scripts for the front end.
     */
    public function registerCTABlock()
    {
        // Check if the register block function exists (If ACF is active)
        if (function_exists('acf_register_block')) {
            acf_register_block([
                'name'            => 'cta',
                'title'           => __('CTA', 'punch-through'),
                'description'     => __('A block for showing CTA module in a cta component.', 'punch-through'),
                'render_template' => PT_THEME_DIR . 'components/blocks/block-cta.php',
                'category'        => 'common',
                'icon'            => 'editor-table',
                'keywords'        => ['cta', 'banner', 'link'],
                'supports'        => [
                    'align' => false,
                ],
            ]);
        }
    }
}
