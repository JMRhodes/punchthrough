<?php
/**
 * Default search form.
 *
 * @package Punch Through
 */
?>

<form role="search" method="get" id="searchform" class="searchform" action="<?php echo get_home_url(); ?>">
    <div class="form-group">
        <div class="input-group">
            <input type="text" class="form-control"
                   placeholder="<?php _e('Search …', 'punch-through') ?>"
                   value="<?php echo get_search_query() ?>" name="s"
                   title="<?php _e('Search for:', 'punch-through') ?>"/>
            <div class="input-group-btn">
                <button class="btn btn--primary btn--sm" type="submit">
                    <?php _e('Search', 'punch-through'); ?>
                </button>
            </div>
        </div>
    </div>
</form>