<?php

namespace PT\App\Posts;

use PT\App\Interfaces\WordPressHooks;
use PT\App\Theme;

/**
 * Class Articles
 *
 * @package PT\App\Posts
 */
class Articles implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('punch-through/posts/format', [$this, 'getPostFormatHeader']);
        add_action('punch-through/archive/header', [$this, 'outputArchiveHeader']);
    }

    /**
     * Include the correct header template for the post format we're currently viewing
     *
     * @param $format
     */
    public function getPostFormatHeader($format)
    {
        $file = locate_template("components/headers/post-{$format}.php");
        if (file_exists($file)) {
            include($file);
        }
    }

    /**
     * when on an archive page, get the title and any category links we'll need to output. Then call our header file.
     */
    public function outputArchiveHeader()
    {
        // $title = Theme::getArchiveTitle();
        $title      = __('Blog', 'punch-through');
        $list_items = wp_list_categories([
            'title_li' => '',
            'number'   => 6,
            'echo'     => 0,
            'orderby'  => 'count',
            'order'    => 'DESC'
        ]);
        if (is_search()) {
            $title      = __('Search', 'punch-through');
            $list_items = sprintf(
                '<li class="cat-item"><span>Results for: %1$s</span></li>',
                get_search_query()
            );
        }

        $file = locate_template("components/headers/archive-header.php");
        if (file_exists($file)) {
            include($file);
        }
    }

    /**
     * Get other posts in the same categories
     *
     * @param int $post_id
     * @param mixed $categories
     * @param string $num_posts
     *
     * @return object
     */
    public static function getRelatedArticles($post_id, $categories, $num_posts)
    {
        // create category string for query
        if (is_array($categories)) {
            $cat_ids = [];
            foreach ($categories as $category) {
                $cat_ids[] = $category->term_id;
            }
            $cat_ids = implode(', ', $cat_ids);
        } else {
            $cat_ids = strval($categories->term_id);
        }
        $args = [
            'post_type'      => 'post',
            'posts_per_page' => $num_posts,
            'no_found_rows'  => true,
            'cat'            => $cat_ids,
            'post__not_in'   => [$post_id]
        ];

        return new \WP_Query($args);
    }

    /**
     * Get post content and build our excerpt length string for archive views.
     *
     * @param int $char_length
     *
     * @return bool|string
     */
    public static function getPostExcerpt($char_length = 150)
    {
        $excerpt = get_the_excerpt();
        $excerpt = strip_shortcodes($excerpt);
        $excerpt = strip_tags($excerpt);
        $excerpt = substr($excerpt, 0, $char_length);
        $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
        $excerpt = $excerpt . '&hellip;';

        return apply_filters('the_excerpt', $excerpt);
    }

    /**
     * Strip content string to the nearest word by character length and return the excerpt
     *
     * @param $content
     * @param int $length
     *
     * @return string|bool
     */
    public static function getContentLimited($content, $length = 120)
    {
        if (! $content) {
            return false;
        }
        // strip tags to avoid breaking any html
        $excerpt = strip_tags($content);
        if (strlen($excerpt) > $length) {
            // truncate string
            $stringCut = substr($excerpt, 0, $length);
            // make sure it ends in a word so assassinate doesn't become ass...
            $excerpt = substr($stringCut, 0, strrpos($stringCut, ' '));
            $excerpt = $excerpt . '&hellip;';
        }

        return $excerpt;
    }
}
