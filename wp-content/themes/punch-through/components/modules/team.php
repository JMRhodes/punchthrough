<?php
/**
 * ACF Module: Team
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$groups = ACF::getRowsLayout('groups', $data);
if (empty($groups)) {
    return;
}
?>

<div id="<?php echo $row_id; ?>" class="module team" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <?php foreach ($groups as $index => $group):
            $headline = ACF::getField('headline', $group);
            $description = ACF::getField('description', $group);
            $color = ACF::getField('color', $group);
            $members = ACF::getRowsLayout('members', $group);

            // set group styles
            $border_color = 'style="border-color: ' . $color . ';"';
            $background_color = 'style="background-color: ' . $color . ';"';
            $text_color = 'style="color: ' . $color . ';"';
            ?>
            <div class="module__row--boxed <?php echo $index === (count($groups) - 1) ? '' : 'module__row--no-end'; ?>"
                <?php echo $border_color; ?>>
                <div class="team__group">
                    <div class="col-md-8 offset-md-2">
                        <?php if ($description) : ?>
                            <div class="module__heading">
                                <h2 class="hdg hdg--2 hdg--light" <?php echo $text_color; ?>>
                                    <?php echo esc_html($headline); ?>
                                </h2>
                            </div>
                        <?php endif; ?>
                        <?php if ($description) : ?>
                            <div class="module__body entry__content">
                                <?php echo apply_filters('the_content', $description); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (! empty($members)) : ?>
                        <div class="members">
                            <?php foreach ($members as $member):
                                $first_name = ACF::getField('first_name', $member);
                                $last_name = ACF::getField('last_name', $member);
                                $title = ACF::getField('title', $member);
                                $image = ACF::getField('image', $member);
                                ?>
                                <div class="member col-6 col-lg-3">
                                    <?php if ($image) : ?>
                                        <div class="member__image" <?php echo $background_color; ?>>
                                            <?php echo Util::getImageHTML(
                                                Media::getAttachmentByID($image),
                                                'featured--md');
                                            ?>
                                        </div>
                                    <?php endif; ?>
                                    <h4 class="member__name">
                                        <?php echo esc_html($first_name) . '<br>' . esc_html($last_name); ?>
                                    </h4>
                                    <?php echo Util::getHTML($title, 'div', ['class' => 'member__title']); ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
