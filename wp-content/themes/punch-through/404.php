<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Punch Through
 */

get_header(); ?>

    <div class="module hero hero--single">
        <div class="container">
            <div class="hero__heading">
                <h1 class="hero__title hdg hdg--1 hdg--light hdg--blue">
                    <?php _e('Page Not Found', 'punch-through'); ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="primary" class="col-sm-8">
                <div class="entry__content article__page">
                    <?php
                    _e(
                        '<p>It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps one of the helpful links below can help.</p>',
                        'punch-through'
                    );

                    wp_nav_menu(
                        [
                            'theme_location'  => 'primary',
                        ]
                    );
                    ?>
                </div><!-- /.entry__content -->
            </div><!-- /#primary -->

            <?php get_sidebar(); ?>
        </div>
    </div>

<?php get_footer();