<?php

namespace PT\App\Interfaces;

/**
 * Interface Hooks provides a contract for classes that add WordPress hooks
 *
 * @package PT\App\Interfaces
 */
interface WordPressHooks
{
    public function addHooks();
}
