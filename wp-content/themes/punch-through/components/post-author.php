<?php
/**
 * Page Component: Post Author
 *
 * @global $data
 */
?>

<h2 class="hdg hdg--light hdg--blue hdg--3">
    <?php _e('About the author', 'punch-through'); ?>
</h2>
<div class="module post-author">
    <?php
    $author_id   = get_post_field('post_author', get_the_ID());
    $author_meta = get_userdata($author_id);
    $bio         = get_the_author_meta('description', $author_id);
    ?>
    <div class="article__avatar">
        <?php echo get_avatar(get_the_author_meta('ID', $author_id), 164); ?>
    </div>
    <div class="article__meta article__meta--full">
        <div class="article__author">
            <?php
            printf(
                '<a href="%2$s" title="Posts by %1$s" rel="author">%1$s</a>',
                esc_html($author_meta->display_name),
                site_url('/author/' . $author_meta->user_nicename)
            );
            ?>
        </div>
        <?php if (! empty($bio)) : ?>
            <div class="article__author-bio">
                <?php echo apply_filters('the_content', $bio); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
