<?php
/**
 * The sidebar containing the main widget area
 *
 * @package Punch Through
 */

use PT\App\Fields\ACF;
use PT\App\Posts\Articles;

$post_id      = get_the_ID();
$meta         = ACF::getPostMeta($post_id);
$categories   = wp_get_post_categories();
$recent_posts = Articles::getRelatedArticles($post_id, $categories, 4);
$format       = ACF::getField('post_format', $meta, 'text');
?>

<aside id="secondary" class="article__sidebar article__sidebar--<?php echo $format; ?> widget-area col-md-3"
       role="complementary">
    <?php if (is_singular('post')) : ?>
        <?php
        $author_id   = get_post_field('post_author', get_the_ID());
        $author_meta = get_userdata($author_id);
        $bio         = get_the_author_meta('description', $author_id);
        ?>
        <div class="article__meta article__meta--sidebar">
            <div class="article__author">
                <div class="article__avatar">
                    <?php echo get_avatar(get_the_author_meta('ID', $author_id), 164); ?>
                </div>
                <?php
                printf(
                    '<a href="%2$s" title="Posts by %1$s" rel="author">%1$s</a>',
                    esc_html($author_meta->display_name),
                    site_url('/author/' . $author_meta->user_nicename)
                );
                ?>
            </div>
            <?php if (! empty($bio)) : ?>
                <div class="article__author-bio">
                    <?php echo apply_filters('the_content', $bio); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php
    if (! empty($recent_posts->posts)) :
        ?>
        <div class="recent-posts">
            <h3 class="hdg hdg--5 recent-posts__headline">
                <?php _e('Recent Posts', 'punch-through'); ?>
            </h3>
            <ul>
                <?php
                while ($recent_posts->have_posts()) {
                    $recent_posts->the_post();

                    printf(
                        '<li class="recent-posts__item">
                    <h4 class="hdg hdg--6 recent-posts__title">
                        <a href="%2$s" title="%1$s">%1$s</a>
                    </h4>
                    <span class="recent-posts__author">%3$s</span>
                    </li>',
                        esc_html(get_the_title()),
                        esc_url(get_permalink()),
                        get_the_author_posts_link()
                    );
                }
                ?>
            </ul>
        </div>
    <?php endif; ?>
</aside><!-- #secondary -->
