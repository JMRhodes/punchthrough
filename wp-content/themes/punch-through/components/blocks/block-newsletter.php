<?php
/**
 * Newsletter Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

use PT\App\Fields\Util;
use PT\App\Fields\ACF;

// Create id attribute allowing for custom "anchor" value.
$row_id = 'newsletter-' . $block['id'];
if (! empty($block['anchor'])) {
    $row_id = $block['anchor'];
}

$data = [
    'block'          => true,
    'headline'       => (get_field('headline') ?: 'Headline'),
    'description'    => (get_field('description') ?: 'Lorem ipsum dolor sit amen.'),
    'form_id'        => (get_field('form_id') ?: false),
    'padding_top'    => (get_field('padding_top') ?: '0'),
    'padding_bottom' => (get_field('padding_bottom') ?: '0')
];

$file = locate_template("components/modules/newsletter.php");
if (file_exists($file)) {
    include($file);
}
