<?php

/*
* Plugin Name: Lktags - Linkedin Insight Tags
* Description: The linkedin conversion Tracking plugin allows to add strategically your linkedin TAG ID on all your webpages (with the base code). No need to edit your theme files!
* Author: Pagup
* Version: 1.1.8
* Author URI: https://pagup.com/
* Text Domain: lktags-linkedin-insight-tags
* Domain Path: /languages/
*/
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !function_exists( 'lktags_fs' ) ) {
    // Create a helper function for easy SDK access.
    function lktags_fs()
    {
        global  $lktags_fs ;
        
        if ( !isset( $lktags_fs ) ) {
            // Include Freemius SDK.
            require_once dirname( __FILE__ ) . '/vendor/freemius/start.php';
            $lktags_fs = fs_dynamic_init( array(
                'id'              => '3402',
                'slug'            => 'lktags-linkedin-insight-tags',
                'type'            => 'plugin',
                'public_key'      => 'pk_acb2cca0e443d00ce5ceea7b0e5c3',
                'is_premium'      => false,
                'premium_suffix'  => 'Linkedin Insight Tag for Woocommerce',
                'has_addons'      => false,
                'has_paid_plans'  => true,
                'has_affiliation' => 'selected',
                'trial'           => array(
                'days'               => 7,
                'is_require_payment' => true,
            ),
                'menu'            => array(
                'slug'           => 'lktags',
                'override_exact' => true,
                'first-path'     => 'options-general.php?page=lktags',
                'support'        => false,
                'parent'         => array(
                'slug' => 'options-general.php',
            ),
            ),
                'is_live'         => true,
            ) );
        }
        
        return $lktags_fs;
    }
    
    // Init Freemius.
    lktags_fs();
    // Signal that SDK was initiated.
    do_action( 'lktags_fs_loaded' );
    function lktags_fs_settings_url()
    {
        return admin_url( 'options-general.php?page=lktags&tab=lktags-settings' );
    }
    
    lktags_fs()->add_filter( 'connect_url', 'lktags_fs_settings_url' );
    lktags_fs()->add_filter( 'after_skip_url', 'lktags_fs_settings_url' );
    lktags_fs()->add_filter( 'after_connect_url', 'lktags_fs_settings_url' );
    lktags_fs()->add_filter( 'after_pending_connect_url', 'lktags_fs_settings_url' );
}

// freemius opt-in
function lktags_fs_custom_connect_message(
    $message,
    $user_first_name,
    $product_title,
    $user_login,
    $site_link,
    $freemius_link
)
{
    $break = "<br><br>";
    return sprintf( esc_html__( 'Hey %1$s, %2$s Click on Allow & Continue to activate Linkedin Insight Tag on your website :)! The Linkedin tag (with base & event codes) allows you to track actions people take on your website. %2$s Never miss an important update -- opt-in to our security and feature updates notifications. %2$s See you on the other side.', 'lktags-linkedin-insight-tags' ), $user_first_name, $break );
}

lktags_fs()->add_filter(
    'connect_message',
    'lktags_fs_custom_connect_message',
    10,
    6
);
class lktags
{
    function __construct()
    {
        // making sure we have the right paths...
        // making sure we have the right paths...
        
        if ( !defined( 'WP_PLUGIN_URL' ) ) {
            if ( !defined( 'WP_CONTENT_DIR' ) ) {
                define( 'WP_CONTENT_DIR', ABSPATH . 'wp-content' );
            }
            if ( !defined( 'WP_CONTENT_URL' ) ) {
                define( 'WP_CONTENT_URL', get_option( 'siteurl' ) . '/wp-content' );
            }
            if ( !defined( 'WP_PLUGIN_DIR' ) ) {
                define( 'WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins' );
            }
            define( 'WP_PLUGIN_URL', WP_CONTENT_URL . '/plugins' );
        }
        
        // end if
        // stuff to do on plugin activation/deactivation
        //register_activation_hook(__FILE__, array(&$this, 'lktags_activate'));
        register_deactivation_hook( __FILE__, array( &$this, 'lktags_deactivate' ) );
        //add quick links to plugin settings
        $plugin = plugin_basename( __FILE__ );
        if ( is_admin() ) {
            add_filter( "plugin_action_links_{$plugin}", array( &$this, 'lktags_setting_link' ) );
        }
    }
    
    // end function __construct()
    // quick setting link in plugin section
    function lktags_setting_link( $links )
    {
        $settings_link = '<a href="options-general.php?page=lktags">Settings</a>';
        array_unshift( $links, $settings_link );
        return $links;
    }
    
    // end function setting_link()
    // register options
    function lktags_options()
    {
        $lktags_options = get_option( 'lktags' );
        return $lktags_options;
    }
    
    // end function lktags_options()
    // removed settings (if checked) on plugin deactivation
    function lktags_deactivate()
    {
        $lktags_options = $this->lktags_options();
        if ( $lktags_options['lktags_remove_settings'] ) {
            delete_option( 'lktags' );
        }
    }

}
// end class
$lktags = new lktags();
function lktags_wp_head()
{
    global  $lktags ;
    $lktags_options = $lktags->lktags_options();
    
    if ( isset( $lktags_options['enable_lktags'] ) && !empty($lktags_options['enable_lktags']) && isset( $lktags_options['lktags_id'] ) && !empty($lktags_options['lktags_id']) ) {
        $tag_id = stripslashes( $lktags_options['lktags_id'] );
        $lktag_base = <<<LKBASE
<!-- Linkedin Insight Base Code --!>
<script type="text/javascript">
_linkedin_partner_id = "{$tag_id}";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid={$tag_id}&fmt=gif" />
</noscript>
<!-- End Linkedin Insight Base Code --!>
LKBASE;
        
        if ( class_exists( 'woocommerce' ) ) {
            if ( !is_singular( 'product' ) && !is_cart() && !is_checkout() ) {
                echo  $lktag_base . "\n" ;
            }
        } else {
            echo  $lktag_base . "\n" ;
        }
        
        //echo "<div display='none'><script type='text/javascript'>console.log('Output: ".$lktags_pagevisit."')</script></div>";
    }

}

add_action( 'wp_head', 'lktags_wp_head' );
// admin notifications
include_once dirname( __FILE__ ) . '/inc/notices.php';
add_action( 'init', 'lktags_textdomain' );
function lktags_textdomain()
{
    load_plugin_textdomain( 'lktags-linkedin-insight-tags', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

if ( is_admin() ) {
    include_once dirname( __FILE__ ) . '/lktags-linkedin-insight-tags-admin.php';
}