<?php
/**
 * ACF Module: Testimonial
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;
use PT\App\Media;

$name    = ACF::getField('name', $data);
$title   = ACF::getField('title', $data);
$message = ACF::getField('message', $data);
$image   = ACF::getField('image', $data);
$logo    = ACF::getField('logo', $data);
$layout  = ACF::getField('layout', $data);
?>

<div id="<?php echo $row_id; ?>" class="module testimonial" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="testimonial__block">
            <div class="col-lg-4 testimonial__image">
                <?php
                echo Util::getImageHTML(
                    Media::getAttachmentByID($image),
                    'featured--md'
                );
                ?>
            </div>
            <div class="col-12 col-lg-8 testimonial__content">
                <div class="testimonial__body">
                    <?php
                    if (! empty($logo)) {
                        echo Util::getImageHTML(
                            Media::getAttachmentByID($logo),
                            'full',
                            [
                                'class' => 'testimonial__logo'
                            ]
                        );
                    }
                    ?>
                    <div class="module__body entry__content">
                        <?php echo apply_filters('the_content', $message); ?>
                    </div>
                    <div class="testimonial__meta">
                        <h4 class="testimonial__name hdg--5">
                            <?php echo esc_html($name); ?>
                        </h4>
                        <span class="testimonial__title">
                            <?php echo esc_html($title); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>