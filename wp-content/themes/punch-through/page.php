<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Punch Through
 */

use PT\App\Fields\Util;

$style = '';

if (has_post_thumbnail()) {
    $thumbnail  = get_post_thumbnail_id();
    $attachment = [
        'background'       => '',
        'background_color' => '#e9e9e9',
        'background_image' => $thumbnail,
        'background_size'  => 'cover'
    ];
    $style      = Util::getInlineStyles($attachment, 'featured--xl');
}

get_header(); ?>

    <div class="module hero hero--single" <?php echo $style; ?>>
        <div class="container">
            <div class="hero__heading">
                <h1 class="hero__title hdg hdg--1 hdg--light hdg--blue">
                    <?php echo the_title(); ?>
                </h1>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div id="primary" class="col-12 col-md-10 offset-md-1">
                <?php
                while (have_posts()) {
                    the_post();
                    // Loads the content/singular/page.php template.
                    get_template_part('content/singular/page');
                }
                ?>
            </div><!-- /#primary -->
        </div>
    </div>

<?php get_footer();