<?php
/**
 * ACF Module: Form
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline = ACF::getField('headline', $data);
$content  = ACF::getField('content', $data);
$form     = ACF::getField('form', $data);
$layout   = ACF::getField('layout', $data);

if (empty($form)) {
    return false;
}
?>

<div id="<?php echo $row_id; ?>" class="module form" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <div class="module__heading">
                    <h2 class="content-area__title hdg hdg--2 hdg--light hdg--blue">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="module__body entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
            </div>
            <div class="col-md-8 offset-md-2 form__embed">
                <?php wpforms_display($form); ?>
            </div>
        </div>
    </div>
</div>