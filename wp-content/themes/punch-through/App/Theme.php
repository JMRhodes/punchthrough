<?php

namespace PT\App;

/**
 * Class Theme
 *
 * @package PT\App
 */
class Theme
{

    /**
     * Retrieve the archive title based on the queried object.
     *
     * @return string Archive title.
     */
    public static function getArchiveTitle()
    {
        if (is_category()) {
            /* translators: Category archive title. %s: Category name */
            $title = single_cat_title('', false);
        } elseif (is_tag()) {
            /* translators: Tag archive title. %s: Tag name */
            $title = single_tag_title('', false);
        } elseif (is_author()) {
            /* translators: Author archive title. %s: Author name */
            $title = get_the_author();
        } elseif (is_year()) {
            /* translators: Yearly archive title. %s: Year */
            $title = sprintf(__('Year: %s'), get_the_date(_x('Y', 'yearly archives date format')));
        } elseif (is_month()) {
            /* translators: Monthly archive title. %s: Month name and year */
            $title = sprintf(__('Month: %s'), get_the_date(_x('F Y', 'monthly archives date format')));
        } elseif (is_day()) {
            /* translators: Daily archive title. %s: Date */
            $title = sprintf(__('Day: %s'), get_the_date(_x('F j, Y', 'daily archives date format')));
        } elseif (is_post_type_archive()) {
            /* translators: Post type archive title. %s: Post type name */
            $title = post_type_archive_title('', false);
        } elseif (is_tax()) {
            $tax = get_taxonomy(get_queried_object()->taxonomy);
            /* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
            $title = sprintf(__('%1$s: %2$s'), $tax->labels->singular_name, single_term_title('', false));
        } else {
            $title = __('Blog', 'punch-through');
        }

        /**
         * Filters the archive title.
         *
         * @param string $title Archive title to be displayed.
         */
        return apply_filters('punch-through/archives/title', $title);
    }
}
