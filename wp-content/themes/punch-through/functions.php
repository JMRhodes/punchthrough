<?php
/**
 * Functions and definitions
 *
 * @package Punch Through
 */

use PT\App\Core\Init;
use PT\App\Setup;
use PT\App\Scripts;
use PT\App\Media;
use PT\App\Fields\ACF;
use PT\App\Fields\Options;
use PT\App\Fields\Modules;
use PT\App\Shortcodes;
use PT\App\Blocks\Newsletter;
use PT\App\Blocks\TableOfContents;
use PT\App\Blocks\CTA;
use PT\App\Posts\Articles;
use PT\App\Posts\Portfolio;
use PT\App\Menus\PrimaryMenuWalker;

/**
 * Define Theme Version
 * Define Theme directories
 */
define('THEME_VERSION', '1.1.0');
define('PT_THEME_DIR', trailingslashit(get_template_directory()));
define('PT_THEME_PATH_URL', trailingslashit(get_template_directory_uri()));

// Require Autoloader
require_once PT_THEME_DIR . 'vendor/autoload.php';

/**
 * Theme Setup
 */
add_action('after_setup_theme', function () {

    (new Init())
        ->add(new Setup())
        ->add(new Scripts())
        ->add(new Options())
        ->add(new Media())
        ->add(new Modules())
        ->add(new Shortcodes())
        ->add(new ACF())
        ->add(new Newsletter())
        ->add(new TableOfContents())
        ->add(new CTA())
        ->add(new Articles())
        ->add(new Portfolio())
        ->add(new PrimaryMenuWalker())
        ->initialize();


    // Translation setup
    load_theme_textdomain('punch-through', PT_THEME_DIR . '/languages');

    // Let WordPress manage the document title.
    add_theme_support('title-tag');

    // Add automatic feed links in header
    add_theme_support('automatic-feed-links');

    // Add Post Thumbnail Image sizes and support
    add_theme_support('post-thumbnails');

    // Switch default core markup to output valid HTML5.
    add_theme_support('html5', [
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption'
    ]);
});
