<?php
/**
 * ACF Module: Stats
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline = ACF::getField('headline', $data);
$content  = ACF::getField('content', $data);
$stats    = ACF::getRowsLayout('stats', $data);
?>

<div id="<?php echo $row_id; ?>" class="module stats">
    <div class="container">
        <div class="stats__wrap">
            <div class="stats__head">
                <div class="module__heading">
                    <h2 class="stats__title hdg hdg--1 hdg--light hdg--yellow">
                        <?php echo esc_html($headline); ?>
                    </h2>
                </div>
                <div class="hdg--5 entry__content">
                    <?php echo apply_filters('the_content', $content); ?>
                </div>
            </div>
            <?php if (! empty($stats)) : ?>
                <div class="stats__grid" <?php echo Util::getInlineStyles($data); ?>>
                    <div class="col-md-10 offset-md-1">
                        <div class="row">
                            <?php
                            foreach ($stats as $stat) {
                                printf(
                                    '<div class="stats__item col-12 col-md-6">
                                        <h3 class="stats__data hdg hdg--1 hdg--light hdg--yellow">%1$s</h3>
                                        <h4 class="stats__label">%2$s</h4>
                                    </div>',
                                    esc_html(ACF::getField('total', $stat, '')),
                                    esc_html(ACF::getField('label', $stat, ''))
                                );
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>