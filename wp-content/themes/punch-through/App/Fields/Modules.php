<?php

namespace PT\App\Fields;

use PT\App\Interfaces\WordPressHooks;
use PT\App\Fields\Options;
use PT\App\Media;

/**
 * Class Modules
 *
 * @package PT\App\Fields
 */
class Modules implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('pt/modules/output', [$this, 'outputFlexibleModules']);
    }

    /**
     * Loop through flexible modules meta and include each module file to the page.
     * $data is set to the scope of just the current module, so that only relevant values are passed to each file.
     *
     * @param $post_id
     */
    public function outputFlexibleModules($post_id)
    {
        $post_id          = $post_id ?: get_the_ID();
        $meta             = ACF::getPostMeta($post_id);
        $parallax         = Options::getSiteOption('parallax_image');
        $parallax_enabled = ACF::getField('parallax_enabled', $meta);

        if ($parallax && $parallax_enabled) {
            $attachment = Media::getAttachmentByID($parallax);
            // insert parallax wrapper
            printf(
                '<div class="jarallax">
                <img class="jarallax-img" src="%1$s" alt="%2$s">',
                esc_url($attachment->url),
                (! empty($attachment->alt) ? $attachment->alt : $attachment->name)
            );
        }

        if (! empty($meta['modules']) && is_array($meta['modules'])) {
            $modules = ACF::getRowsLayout('modules', $meta);

            foreach ($meta['modules'] as $index => $module) {
                $data   = $modules[$index];
                $row_id = $module . '-' . $index;

                $file = locate_template("components/modules/{$module}.php");
                if (file_exists($file)) {
                    include($file);
                }

                if (0 === $index && 'hero' === $module) {
                    $file = locate_template("components/page-content.php");
                    if (file_exists($file)) {
                        include($file);
                    }
                }
            }
        }

        // end parallax wrapper
        if ($parallax && $parallax_enabled) {
            echo '</div>';
        }
    }
}
