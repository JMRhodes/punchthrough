<?php
/**
 * Archive: Blog Header
 *
 * @global $title
 * @global $list_items
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

?>

<div class="blog-header">
    <div class="blog-header__title">
        <h1 class="hdg hdg--2 hdg--light hdg--dark-blue">
            <?php echo esc_html($title); ?>
        </h1>
    </div>
    <div class="blog-header__categories">
        <?php
        if (! empty($list_items)) {
            echo '<ul class="cat-menu">';
            echo $list_items;
            echo '</ul>';
        }
        ?>
    </div>
</div>
