<?php
/**
 * @package Punch Through
 */

use PT\App\Fields\ACF;

$meta        = ACF::getPostMeta(get_the_ID());
$format      = ACF::getField('post_format', $meta, 'text');
$author_id   = get_post_field('post_author', get_the_ID());
$author_meta = get_userdata($author_id);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('post-format--' . $format); ?>>
    <?php
    // only include the post header inside content if our format is image.
    if ('image' === $format || 'hybrid' === $format || 'full' === $format) {
        $file = locate_template("components/headers/post-header.php");
        if (file_exists($file)) {
            include($file);
        }
    }
    ?>

    <div class="entry__content entry__content--article <?php echo 'full' === $format ? 'col-md-9' : ''; ?>">
        <?php if ('banner' === $format) : ?>
            <div class="article__date-author">
                <span class="article__date"><?php echo get_the_date(); ?></span>
                <?php
                printf(
                    'By: <a href="%2$s" class="hdg--yellow" title="Posts by %1$s" rel="author">%1$s</a>',
                    esc_html($author_meta->display_name),
                    site_url('/author/' . $author_meta->user_nicename)
                );
                ?>
            </div>
        <?php endif; ?>

        <?php the_content(); ?>

        <?php
        if ('full' === $format) {
            $file = locate_template("components/post-author.php");
            if (file_exists($file)) {
                include($file);
            }
        }
        ?>
    </div><!-- .entry__content -->
</article><!-- #post-## -->
