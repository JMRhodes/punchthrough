<?php

namespace PT\App\Blocks;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Newsletter
 *
 * @package Punch Through\App\Blocks
 */
class Newsletter implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('acf/init', [$this, 'registerNewsletterBlock']);
    }

    /**
     * Load scripts for the front end.
     */
    public function registerNewsletterBlock()
    {
        // Check if the register block function exists (If ACF is active)
        if (function_exists('acf_register_block')) {
            acf_register_block([
                'name'            => 'newsletter',
                'title'           => __('Newsletter', 'punch-through'),
                'description'     => __('A block for showing Newsletter module in a form component.', 'punch-through'),
                'render_template' => PT_THEME_DIR . 'components/blocks/block-newsletter.php',
                'category'        => 'common',
                'icon'            => 'editor-table',
                'keywords'        => ['newsletter', 'banner', 'link'],
                'supports'        => [
                    'align' => false,
                ],
            ]);
        }
    }
}
