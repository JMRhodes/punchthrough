<?php
/**
 * ACF Module: Content Area
 *
 * @global $data
 * @global $row_id
 */

use PT\App\Fields\ACF;
use PT\App\Fields\Util;

$headline    = ACF::getField('headline', $data);
$content     = ACF::getField('content', $data);
$layout      = ACF::getField('layout', $data);
$boxed_color = ACF::getField('boxed_color', $data, 'blue');
$layout      = ('boxed' === $layout ? $layout . ' content-area__row--' . $boxed_color : $layout);
?>

<div id="<?php echo $row_id; ?>" class="module content-area" <?php echo Util::getInlineStyles($data); ?>>
    <div class="container">
        <div class="content-area__row--<?php echo $layout; ?>">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <div class="module__heading">
                        <h2 class="content-area__title hdg hdg--2 hdg--light hdg--blue">
                            <?php echo esc_html($headline); ?>
                        </h2>
                    </div>
                    <div class="module__body entry__content">
                        <?php echo apply_filters('the_content', $content); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>