<?php

namespace PT\App\Posts;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class Portfolio
 *
 * @package PT\App\Posts
 */
class Portfolio implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('init', [$this, 'registerPostTypePortfolio']);
    }

    /**
     * Register our Portfolio post type
     *
     * @param $format
     */
    public function registerPostTypePortfolio()
    {
        PostTypes::registerPostType(
            'portfolio',
            __('Portfolio', 'punch-through'),
            __('Portfolio', 'punch-through'),
            [
                'has_archive' => false,
                'supports'    => ['title', 'thumbnail'],
                'menu_icon'   => 'dashicons-category'
            ]
        );
    }
}
