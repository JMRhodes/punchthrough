<div class="lktags-column col-3 lktags-txt">

    <div class="lktags-sidebar">


        <div class="box">
            <h2><?php echo __( 'Subscribe & get a 10% OFF', 'lktags' ); ?></h2>
            <p><?php echo __( 'Enter your email to get our SEO best practice guide AND a 10% coupon code on Linkedin Insight Tag PRO plugin.', 'lktags' ); ?></p>
            <!-- Begin MailChimp Signup Form -->

            <form action="https://Pagup.us14.list-manage.com/subscribe/post?u=a706b8e968389b05725c65849&amp;id=59ebd61587" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

                <input type="email" value="" name="EMAIL" class="lktags-field" placeholder="<?php echo __( 'Email address', 'lktags' ); ?>" required>

                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_a706b8e968389b05725c65849_59ebd61587" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="<?php echo __( 'Subscribe', 'lktags' ); ?>" name="subscribe" id="mc-embedded-subscribe" class="lktags-btn"></div>

            </form><br />
            <!--End Mailchimp-->

        </div>

        <div class="box">
            <h2><?php echo __( 'BOOST YOUR RANKING', 'lktags' ); ?></h2>
            <p><?php echo __( 'Optimize your Robots.txt with Better Robots.txt (made by PAGUP) & Boost your ranking on search engines.', 'lktags' ); ?></p>
            <p><a href="https://wordpress.org/plugins/better-robots-txt/" class="lktags-btn"><?php echo __( 'Try Better Robots.txt &raquo;', 'lktags' ); ?></a></p>
        </div>

        <div class="box">
            <h2><?php echo __( 'Optimize your ALT TEXT', 'lktags' ); ?></h2>
            <p><?php echo __( 'Auto optimize all image alt texts (including Woocommerce online store), of your pages, posts & product, from Yoast SEO optimization settings (keywords) and/or Post title.', 'lktags' ); ?></p>
            <p><a href="https://wordpress.org/plugins/bulk-image-alt-text-with-yoast/" target="_blank" class="lktags-btn"><?php echo __( 'Get BIALTY by Pagup &raquo;', 'lktags' ); ?></a></p>
        </div>

        <div class="box">
            <h2><?php echo __( 'Pinterest event tracking', 'lktags' ); ?></h2>
            <p><?php echo __( 'Pinterest conversion tags plugin allows to add strategically your Pinterest TAG ID on all your webpages (with the base code). No need to edit your theme files!', 'lktags' ); ?></p>
            <p><a href="https://wordpress.org/plugins/add-pinterest-conversion-tags/" target="_blank" class="lktags-btn"><?php echo __( 'Get PCTAGS by Pagup &raquo;', 'lktags' ); ?></a></p>
        </div>

    </div>

</div>