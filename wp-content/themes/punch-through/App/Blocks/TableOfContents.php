<?php

namespace PT\App\Blocks;

use PT\App\Interfaces\WordPressHooks;

/**
 * Class TableOfContents
 *
 * @package Punch Through\App\Blocks
 */
class TableOfContents implements WordPressHooks
{

    /**
     * Add class hooks.
     */
    public function addHooks()
    {
        add_action('acf/init', [$this, 'registerTableOfContentsBlock']);
    }

    /**
     * Load scripts for the front end.
     */
    public function registerTableOfContentsBlock()
    {
        // Check if the register block function exists (If ACF is active)
        if (function_exists('acf_register_block')) {
            acf_register_block([
                'name'            => 'table-of-contents',
                'title'           => __('Table of Contents', 'punch-through'),
                'description'     => __(
                    'A block for showing Table of Contents module in an article component.',
                    'punch-through'
                ),
                'render_template' => PT_THEME_DIR . 'components/blocks/block-table-of-contents.php',
                'category'        => 'common',
                'icon'            => 'editor-table',
                'keywords'        => ['table of contents', 'toc', 'glossary', 'index'],
                'supports'        => [
                    'align' => false,
                ],
            ]);
        }
    }
}
