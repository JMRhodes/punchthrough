<?php
/**
 * Table of Contents Block Template.
 *
 * @param array $block The block settings and attributes.
 * @param string $content The block inner HTML (empty).
 * @param bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

use PT\App\Fields\Util;
use PT\App\Fields\ACF;

// Create id attribute allowing for custom "anchor" value.
$row_id = 'toc-' . $block['id'];

if (! empty($block['anchor'])) {
    $row_id = $block['anchor'];
}

$meta = ACF::getField('data', $block, []);
?>

<div id="<?php echo esc_attr($row_id); ?>" class="block__toc toc">
    <a href="javascript:void(0);" class="toc__open">
        <img src="<?php echo PT_THEME_PATH_URL . '/assets/images/list.svg'; ?>"/>
    </a>

    <div class="toc__body">
        <div class="module__heading">
            <span class="toc__title">
                <?php echo esc_html(ACF::getField('headline', $meta)); ?>

                <a href="javascript:void(0);" class="toc__close">
                    <img src="<?php echo PT_THEME_PATH_URL . '/assets/images/arrow_left.svg'; ?>"/>
                </a>
            </span>

            <ul class="toc__list" data-type="list"></ul>

            <span class="toc__read">
                <?php echo do_shortcode('[rt_reading_time postfix="minute read" postfix_singular="minute read"]'); ?>
            </span>
        </div>
    </div>
</div>
