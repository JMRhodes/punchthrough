/**
 *  Initialize blockquotesSlider
 */
const blockquotesSlider = (function($) {

  const pub         = {}; // public facing functions
  const blockquotes = $('.quotes');

  pub.init = function() {

    if (blockquotes.length > 0) {
      pub.initSlickSlider();
    }
  };

  /**
   * Initialize slick slider on blockquote items
   */
  pub.initSlickSlider = function() {
    blockquotes.slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      adaptiveHeight: true,
      dotsClass: 'quotes__dots',
      arrows: false
    });
  };

  return pub;

})(jQuery);