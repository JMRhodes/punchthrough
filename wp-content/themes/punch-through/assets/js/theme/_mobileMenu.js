/**
 *  Initialize mobile menu toggle
 */
const mobileMenu = (function() {

  const pub           = {}; // public facing functions
  const menuToggle    = document.querySelector('div[data-toggle="menu"]');
  const menuContainer = document.querySelector('.mobile-menu');
  const siteHeader    = document.querySelector('.header');

  pub.init = function() {
    this._listenForMobileToggle();
  };

  pub._listenForMobileToggle = function() {
    menuToggle.addEventListener(
      'click',
      this._toggleMobileMenu
    );
  };

  pub._toggleMobileMenu = function() {
    if (siteHeader.classList.contains('header--fixed')) {
      siteHeader.classList.remove('header--fixed');
    } else {
      setTimeout(function() {
        siteHeader.classList.add('header--fixed');
      }, 300);
    }
    menuToggle.classList.toggle('menu-icon--opened');
    menuContainer.classList.toggle('mobile-menu--active');
  };

  return pub;

}());
